# -*- coding: utf-8 -*-
# Standard Library
import json
import os
import shutil
from pathlib import Path
from typing import List

import pytest

from agent.app import create_app


def create_files(
    root_directory: Path,
    type: str,
    content: dict[str, str],
) -> Path:
    parent = Path(root_directory, type)
    parent.mkdir(parents=True, exist_ok=True)

    for filename in content.keys():
        file = Path(parent, filename)
        if type == "txt":
            file.write_text(content[filename])
        elif type == "json":
            with open(file, 'w') as f:
                json.dump({"content": content[filename]}, f)

    return parent


@pytest.fixture(scope="session")
def create_directory(tmp_path_factory) -> Path:
    """
    Fixture that creates directories in the tmp_path/tmp directory,
    and returns the directory.
    """
    tmpdir = tmp_path_factory.mktemp("tmp", numbered=True)

    yield tmpdir

    shutil.rmtree(str(tmpdir))


@pytest.fixture(scope="function", autouse=True)
def create_directory_hierarchy(create_directory) -> List[str]:
    txt_parent = create_files(root_directory=create_directory, type="txt",
                              content={"foo.txt": "foo", "bar.txt": "bar", "baz.txt": "baz"})
    json_parent = create_files(root_directory=create_directory, type="txt",
                               content={"foo.json": "foo", "bar.json": "bar", "baz.json": "baz"})
    yield [str(txt_parent.absolute()), str(json_parent.absolute())]


@pytest.fixture(scope="function", autouse=True)
def create_cert_and_public_files(create_directory) -> List[str]:
    pem_file = Path(create_directory, "chain.pem")
    shutil.copy(Path(Path(__file__).parent, "data", "abc-tls.crt"), pem_file)

    public_directory = Path(create_directory, "public")
    public_directory.mkdir(parents=True, exist_ok=True)
    yield [str(pem_file.absolute()), str(public_directory.absolute())]


@pytest.fixture()
def app():
    app = create_app()

    root_directory = Path(app.config["UPLOAD_FOLDER"])
    root_directory.mkdir(parents=True, exist_ok=True)

    create_files(root_directory=root_directory, type="txt",
                 content={"foo.txt": "foo", "bar.txt": "bar", "baz.txt": "baz"})

    app.config.update({
        "TESTING": True,
    })

    yield app

    subfolders = [f.path for f in os.scandir(root_directory) if f.is_dir()]
    for folder in subfolders:
        shutil.rmtree(str(folder))


@pytest.fixture()
def client(app):
    return app.test_client()


@pytest.fixture()
def runner(app):
    return app.test_cli_runner()
