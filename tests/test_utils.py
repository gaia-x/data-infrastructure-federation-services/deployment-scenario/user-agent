# -*- coding: utf-8 -*-
import hashlib
import http
import json
import os
from pathlib import Path
from uuid import UUID

import pytest
from agent.config import settings, ALLOWED_OBJECT_ONTOLOGY, VALID_OBJECT_DIRECTORIES
from agent.utils import (
    is_valid_store_object_parameter,
    get_id_from_jsonld,
    X_API_KEY_HEADER,
    api_key_required,
    get_type_from_jsonld,
    compute_sha_hash_value,
    HashType,
    save_on_disk_by_filepath,
    InvalidDataError,
    clean_output_directories, generate_object_id, is_known_ontology, extract_first_object_type,
    extract_information_from_object_id, extract_known_object_type,
)


class TestIsValidStoreObjectParameter:
    #  Tests that the function returns True when given a valid path and ontology_type
    def test_valid_path_and_ontology_type(self):
        assert is_valid_store_object_parameter("vc", "VerifiableCredential") is True
        assert is_valid_store_object_parameter("vp", "VerifiablePresentation") is True
        assert (
            is_valid_store_object_parameter("service-offering", "ServiceOffering")
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "self-assessed-compliance-criteria-claim",
                "SelfAssessedComplianceCriteriaClaim",
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "self-assessed-compliance-criteria-credential",
                "SelfAssessedComplianceCriteriaCredential",
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "located-service-offering", "LocatedServiceOffering"
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "compliance-reference", "ComplianceReference"
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "compliance-criterion", "ComplianceCriterion"
            )
            is True
        )
        assert (
            is_valid_store_object_parameter("compliance-label", "ComplianceLabel")
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "compliance-certification-scheme", "ComplianceCertificationScheme"
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "compliance-certificate-credential", "ComplianceCertificateCredential"
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "compliance-certificate-claim", "ComplianceCertificateClaim"
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "third-party-compliance-certificate-credential",
                "ThirdPartyComplianceCertificateCredential",
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "third-party-compliance-certification-scheme",
                "ThirdPartyComplianceCertificationScheme",
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "third-party-compliance-certificate-claim",
                "ThirdPartyComplianceCertificateClaim",
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "compliance-assessment-body", "ComplianceAssessmentBody"
            )
            is True
        )
        assert is_valid_store_object_parameter("data-product", "DataProduct") is True
        assert (
            is_valid_store_object_parameter("legal-participant", "LegalParticipant")
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "gaiax-terms-and-conditions", "GaiaXTermsAndConditions"
            )
            is True
        )

    #  Tests that the function returns True when given a valid path and an empty ontology_type
    def test_valid_path_and_empty_ontology_type(self):
        assert is_valid_store_object_parameter("vc", "") is False
        assert is_valid_store_object_parameter("vc", None) is False

    #  Tests that the function returns True when given a valid ontology_type and an empty path
    def test_valid_ontology_type_and_empty_path(self):
        assert is_valid_store_object_parameter("", "VerifiableCredential") is False
        assert is_valid_store_object_parameter(None, "VerifiableCredential") is False

    #  Tests that the function returns True when given a valid path and ontology_type with special characters
    def test_valid_path_and_ontology_type_with_special_characters(self):
        assert (
            is_valid_store_object_parameter("valid_path!@#", "valid_ontology_type!@#")
            is False
        )

    #  Tests that the function returns True when given a valid path and ontology_type with numbers
    def test_valid_path_and_ontology_type_with_numbers(self):
        assert (
            is_valid_store_object_parameter("valid_path123", "valid_ontology_type123")
            is False
        )

    # Tests that the function returns True when given a valid path and
    # ontology_type with upper and lower case characters
    def test_valid_path_and_ontology_type_with_upper_and_lower_case_characters(self):
        assert is_valid_store_object_parameter("Vc", "VerifiableCredential") is True
        assert is_valid_store_object_parameter("vP", "VerifiablePresentation") is True
        assert (
            is_valid_store_object_parameter("seRvice-offering", "ServiceOffering")
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "selF-assessed-compliance-criteria-claim",
                "SelfAssessedComplianceCriteriaClaim",
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "self-Assessed-compliance-criteria-credential",
                "SelfAssessedComplianceCriteriaCredential",
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "locateD-service-offering", "LocatedServiceOffering"
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "compliaNce-reference", "ComplianceReference"
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "compliance-criterion", "ComplianceCriterion"
            )
            is True
        )
        assert (
            is_valid_store_object_parameter("compliancE-label", "ComplianceLabel")
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "compliance-cErtification-scheme", "ComplianceCertificationScheme"
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "compliance-certIficate-credential", "ComplianceCertificateCredential"
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "compliance-certIficate-claim", "ComplianceCertificateClaim"
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "third-party-compLiance-certificate-credential",
                "ThirdPartyComplianceCertificateCredential",
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "third-party-compliAnce-certification-scheme",
                "ThirdPartyComplianceCertificationScheme",
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "third-party-complianCe-certificate-claim",
                "ThirdPartyComplianceCertificateClaim",
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "compliance-assessmenT-body", "ComplianceAssessmentBody"
            )
            is True
        )
        assert is_valid_store_object_parameter("data-produCt", "DataProduct") is True
        assert (
            is_valid_store_object_parameter("legal-partiCipant", "LegalParticipant")
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "gaiaX-terms-and-condItions", "GaiaXTermsAndConditions"
            )
            is True
        )

    #  Tests that the function returns False when given a valid path is not string and ontology_type is string
    def test_invalid_path_type_and_valid_ontology_type(self):
        assert is_valid_store_object_parameter(123, "VerifiableCredential") is False
        assert is_valid_store_object_parameter([], "VerifiableCredential") is False
        assert is_valid_store_object_parameter({}, "VerifiableCredential") is False
        assert is_valid_store_object_parameter(True, "VerifiableCredential") is False

    #  Tests that the function returns False when given a valid path is string and ontology_type is not string
    def test_valid_path_type_and_invalid_ontology_type(self):
        assert is_valid_store_object_parameter("vc", 123) is False
        assert is_valid_store_object_parameter("vc", []) is False
        assert is_valid_store_object_parameter("vc", {}) is False
        assert is_valid_store_object_parameter("vc", True) is False

    #  Tests that the function returns True when given a valid path is string and ontology_type contains a prefix (xx:)
    def test_valid_path_and_ontology_type_with_prefix(self):
        assert (
            is_valid_store_object_parameter("service-offering", "gx:ServiceOffering")
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "self-assessed-compliance-criteria-claim",
                "gx-compliance:SelfAssessedComplianceCriteriaClaim",
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "self-assessed-compliance-criteria-credential",
                "gx-compliance:SelfAssessedComplianceCriteriaCredential",
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "located-service-offering", "gx-participant:LocatedServiceOffering"
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "compliance-reference", "gx-compliance:ComplianceReference"
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "compliance-criterion", "gx-compliance:ComplianceCriterion"
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "compliance-label", "gx-compliance:ComplianceLabel"
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "compliance-certification-scheme",
                "gx-compliance:ComplianceCertificationScheme",
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "compliance-certificate-credential",
                "gx-compliance:ComplianceCertificateCredential",
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "compliance-certificate-claim",
                "gx-compliance:ComplianceCertificateClaim",
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "third-party-compliance-certificate-credential",
                "gx-compliance:ThirdPartyComplianceCertificateCredential",
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "third-party-compliance-certification-scheme",
                "gx-compliance:ThirdPartyComplianceCertificationScheme",
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "third-party-compliance-certificate-claim",
                "gx-compliance:ThirdPartyComplianceCertificateClaim",
            )
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "compliance-assessment-body", "gx-compliance:ComplianceAssessmentBody"
            )
            is True
        )
        assert (
            is_valid_store_object_parameter("data-product", "gx-data:DataProduct")
            is True
        )
        assert (
            is_valid_store_object_parameter("legal-participant", "gx:LegalParticipant")
            is True
        )
        assert (
            is_valid_store_object_parameter(
                "gaiax-terms-and-conditions", "gx-GaiaXTermsAndConditions"
            )
            is False
        )


class TestGetIdFromJsonld:
    #  Should return the value of "@id" key if it exists in the input dictionary
    def test_return_value_of_at_id_key_if_exists(self):
        # Given
        jsonld = {"@id": "123"}

        # When
        result = get_id_from_jsonld(jsonld)

        # Then
        assert result == "123"

    #  Should return the value of "id" key if "@id" key does not exist in the input dictionary
    def test_return_value_of_id_key_if_at_id_key_does_not_exist(self):
        # Given
        jsonld = {"id": "456"}

        # When
        result = get_id_from_jsonld(jsonld)

        # Then
        assert result == "456"

    #  Should return None if the input dictionary is None
    def test_return_none_if_input_dictionary_is_none(self):
        # Given
        jsonld = None

        # When
        result = get_id_from_jsonld(jsonld)

        # Then
        assert result is None

    #  Should return None if the input dictionary is empty
    def test_return_none_if_input_dictionary_is_empty(self):
        # Given
        jsonld = {}

        # When
        result = get_id_from_jsonld(jsonld)

        # Then
        assert result is None

    #  Should return None if the input dictionary does not have "@id" or "id" keys
    def test_return_none_if_input_dictionary_does_not_have_at_id_or_id_keys(self):
        # Given
        jsonld = {"name": "John"}

        # When
        result = get_id_from_jsonld(jsonld)

        # Then
        assert result is None

    #  Should return None if the value of "@id" or "id" key is None
    def test_return_none_if_value_of_at_id_or_id_key_is_none(self):
        # Given
        jsonld = {"@id": None}

        # When
        result = get_id_from_jsonld(jsonld)

        # Then
        assert result is None

    #  Should not handle nested dictionaries with "@id" or "id" keys
    def test_handle_nested_dictionaries_with_at_id_or_id_keys(self):
        # Given
        jsonld = {"data": {"@id": "789"}}

        # When
        result = get_id_from_jsonld(jsonld)

        # Then
        assert result is None

    #  Should handle dictionaries with other keys besides "@id" or "id"
    def test_handle_dictionaries_with_other_keys_besides_at_id_or_id(self):
        # Given
        jsonld = {"name": "John", "age": 30}

        # When
        result = get_id_from_jsonld(jsonld)

        # Then
        assert result is None

    #  Should handle dictionaries with non-string values for "@id" or "id" keys
    def test_handle_dictionaries_with_non_string_values_for_at_id_or_id_keys(self):
        # Given
        jsonld = {"@id": 123}

        # When
        result = get_id_from_jsonld(jsonld)

        # Then
        assert result is None


class TestGetTypeFromJsonld:
    #  Should return the value of "@id" key if it exists in the input dictionary
    def test_return_value_of_at_id_key_if_exists(self):
        # Given
        jsonld = {"@type": "123"}

        # When
        result = get_type_from_jsonld(jsonld)

        # Then
        assert result == ["123"]

    #  Should return the value of "id" key if "@id" key does not exist in the input dictionary
    def test_return_value_of_id_key_if_at_id_key_does_not_exist(self):
        # Given
        jsonld = {"type": "456"}

        # When
        result = get_type_from_jsonld(jsonld)

        # Then
        assert result == ["456"]

    #  Should return None if the input dictionary is None
    def test_return_none_if_input_dictionary_is_none(self):
        # Given
        jsonld = None

        # When
        result = get_type_from_jsonld(jsonld)

        # Then
        assert result is None

    #  Should return None if the input dictionary is empty
    def test_return_none_if_input_dictionary_is_empty(self):
        # Given
        jsonld = {}

        # When
        result = get_type_from_jsonld(jsonld)

        # Then
        assert result is None

    #  Should return None if the input dictionary does not have "@id" or "id" keys
    def test_return_none_if_input_dictionary_does_not_have_at_id_or_id_keys(self):
        # Given
        jsonld = {"name": "John"}

        # When
        result = get_type_from_jsonld(jsonld)

        # Then
        assert result is None

    #  Should return None if the value of "@id" or "id" key is None
    def test_return_none_if_value_of_at_id_or_id_key_is_none(self):
        # Given
        jsonld = {"@type": None}

        # When
        result = get_type_from_jsonld(jsonld)

        # Then
        assert result is None

    #  Should not handle nested dictionaries with "@id" or "id" keys
    def test_handle_nested_dictionaries_with_at_id_or_id_keys(self):
        # Given
        jsonld = {"data": {"@type": "789"}}

        # When
        result = get_type_from_jsonld(jsonld)

        # Then
        assert result is None

    #  Should handle dictionaries with other keys besides "@id" or "id"
    def test_handle_dictionaries_with_other_keys_besides_at_id_or_id(self):
        # Given
        jsonld = {"name": "John", "age": 30}

        # When
        result = get_type_from_jsonld(jsonld)

        # Then
        assert result is None

    #  Should handle dictionaries with non-string values for "@id" or "id" keys
    def test_handle_dictionaries_with_non_string_values_for_at_id_or_id_keys(self):
        # Given
        jsonld = {"@type": 123}

        # When
        result = get_type_from_jsonld(jsonld)

        # Then
        assert result is None


class TestApiKeyRequired:
    #  Function returns the result of the decorated function when a valid API key is provided in the request header.
    def test_valid_api_key_provided(self):
        # Given
        from flask import Flask, jsonify

        app = Flask(__name__)
        api_key = "this-is-a-secure-key"

        @app.route("/protected")
        @api_key_required
        def protected_route():
            return jsonify({"message": "Success"})

        # When
        with app.test_client() as client:
            response = client.get(
                "/protected", headers={X_API_KEY_HEADER: api_key}
            )

        # Then
        assert response.status_code == http.HTTPStatus.OK
        assert response.json == {"message": "Success"}

    #  Function returns a 401 error with a message when no API key is provided in the request header.
    def test_no_api_key_provided(self):
        # Given
        from flask import Flask, jsonify

        app = Flask(__name__)

        @app.route("/protected")
        @api_key_required
        def protected_route():
            return jsonify({"message": "Success"})

        # When
        with app.test_client() as client:
            response = client.get("/protected")

        # Then
        assert response.status_code == 401
        assert response.json == {"message": "a valid api key is missing"}

    #  Function returns a 401 error with a message when an invalid API key is provided in the request header.
    def test_invalid_api_key_provided(self):
        # Given
        from flask import Flask, jsonify

        app = Flask(__name__)

        @app.route("/protected")
        @api_key_required
        def protected_route():
            return jsonify({"message": "Success"})

        # When
        with app.test_client() as client:
            response = client.get(
                "/protected", headers={X_API_KEY_HEADER: "invalid-key"}
            )

        # Then
        assert response.status_code == 401
        assert response.json == {"message": "api key is invalid"}

    #  Function logs a warning when an invalid API key is provided in the request header.
    def test_warning_logged_for_invalid_api_key(self, caplog):
        # Given
        from flask import Flask, jsonify

        app = Flask(__name__)

        @app.route("/protected")
        @api_key_required
        def protected_route():
            return jsonify({"message": "Success"})

        # When
        with app.test_client() as client:
            response = client.get(
                "/protected", headers={X_API_KEY_HEADER: "invalid-key"}
            )

        # Then
        assert response.status_code == 401
        assert response.json == {"message": "api key is invalid"}
        assert f"'invalid-key' != '{settings.api_key_authorized}'" in caplog.text

    #  Function properly decorates the input function and returns a callable.
    def test_decorator_returns_callable(self):
        # Given
        def input_function():
            return "Hello, World!"

        # When
        decorated_function = api_key_required(input_function)

        # Then
        assert callable(decorated_function)

    #  settings.api_key_authorized is an empty string.
    def test_empty_api_key_authorized(self):
        # Given
        from flask import Flask, jsonify

        app = Flask(__name__)

        @app.route("/protected")
        @api_key_required
        def protected_route():
            return jsonify({"message": "Success"})

        # When
        with app.test_client() as client:
            response = client.get("/protected", headers={X_API_KEY_HEADER: ""})

        # Then
        assert response.status_code == 401
        assert response.json == {"message": "api key is invalid"}

    #  settings.api_key_authorized is a string containing only whitespace characters.
    def test_whitespace_api_key_authorized(self):
        # Given
        from flask import Flask, jsonify

        app = Flask(__name__)

        @app.route("/protected")
        @api_key_required
        def protected_route():
            return jsonify({"message": "Success"})

        # When
        with app.test_client() as client:
            response = client.get("/protected", headers={X_API_KEY_HEADER: "   "})

        # Then
        assert response.status_code == 401
        assert response.json == {"message": "api key is invalid"}

    #  settings.api_key_authorized is a string containing non-ASCII characters.
    def test_non_ascii_api_key_authorized(self):
        # Given
        from flask import Flask, jsonify

        app = Flask(__name__)

        @app.route("/protected")
        @api_key_required
        def protected_route():
            return jsonify({"message": "Success"})

        # When
        with app.test_client() as client:
            response = client.get("/protected", headers={X_API_KEY_HEADER: "ñöñ-åśçîî"})

        # Then
        assert response.status_code == 401
        assert response.json == {"message": "api key is invalid"}

    #  X_API_KEY_HEADER is not present in the request headers.
    def test_x_api_key_header_not_present(self):
        # Given
        from flask import Flask, jsonify

        app = Flask(__name__)

        @app.route("/protected")
        @api_key_required
        def protected_route():
            return jsonify({"message": "Success"})

        # When
        with app.test_client() as client:
            response = client.get("/protected")

        # Then
        assert response.status_code == 401
        assert response.json == {"message": "a valid api key is missing"}

    #  X_API_KEY_HEADER is present in the request headers, but its value is an empty string.
    def test_x_api_key_header_empty(self):
        # Given
        from flask import Flask, jsonify

        app = Flask(__name__)

        @app.route("/protected")
        @api_key_required
        def protected_route():
            return jsonify({"message": "Success"})

        # When
        with app.test_client() as client:
            response = client.get("/protected", headers={X_API_KEY_HEADER: ""})

        # Then
        assert response.status_code == 401
        assert response.json == {"message": "api key is invalid"}


class TestComputeShaHashValue:
    #  Computes SHA hash value of a byte string
    def test_computes_sha_hash_value(self):
        # Given
        field = b"test"
        expected_hash = hashlib.sha256(field).hexdigest()

        # When
        actual_hash = compute_sha_hash_value(field)

        # Then
        assert actual_hash == expected_hash

    #  Returns the hash value as a hexadecimal string
    def test_compute_sha_hash_value_returns_hexadecimal_string(self):
        # Given
        field = b"test"
        hash_type = HashType.SHA256

        # When
        result = compute_sha_hash_value(field, hash_type)

        # Then
        assert isinstance(result, str)
        assert all(c in "0123456789abcdef" for c in result)

    #  Supports SHA256, SHA512, SHA1 and MD5 hash types
    def test_compute_sha_hash_value_with_sha256_hash_type(self):
        # Given
        field = b"test"
        expected_hash = hashlib.sha256(field).hexdigest()

        # When
        actual_hash = compute_sha_hash_value(field, HashType.SHA256)

        # Then
        assert actual_hash == expected_hash

    #  Raises TypeError if input is not of type bytes
    def test_raises_type_error_if_input_not_bytes(self):
        # Given
        field = "not_bytes"
        hash_type = HashType.SHA256

        # When, Then
        with pytest.raises(TypeError):
            compute_sha_hash_value(field, hash_type)

    #  Returns correct hash value for valid input and hash type
    def test_correct_hash_value(self):
        # Given
        field = b"Hello World"
        hash_type = HashType.SHA256

        # When
        result = compute_sha_hash_value(field, hash_type)

        # Then
        assert isinstance(result, str)
        assert len(result) == 64
        assert (
            result == "a591a6d40bf420404a011733cfb7b190d62c65bf0bcda32b57b277d9ad9f146e"
        )

    #  Raises TypeError for invalid hash type
    def test_raises_type_error_for_invalid_hash_type(self):
        # Given
        field = b"test"
        hash_type = "INVALID_HASH_TYPE"

        # When, Then
        with pytest.raises(TypeError):
            compute_sha_hash_value(field, hash_type)

    #  Raises TypeError for None input
    def test_raises_type_error_for_none_input(self):
        # Given
        field = None
        hash_type = HashType.SHA256

        # When, Then
        with pytest.raises(TypeError):
            compute_sha_hash_value(field, hash_type)

    #  Raises TypeError for non-bytes input
    def test_raises_type_error_for_non_bytes_input(self):
        # Given a non-bytes input
        field = "hello"

        # When compute_sha_hash_value is called with the non-bytes input
        # Then it should raise a TypeError
        with pytest.raises(TypeError):
            compute_sha_hash_value(field)

    #  Supports custom hash types
    def test_custom_hash_type(self):
        # Given
        field = b"Hello, World!"
        hash_type = HashType("SHA1")

        # When
        result = compute_sha_hash_value(field, hash_type)

        # Then
        assert result == hashlib.sha1(field).hexdigest()

    #  Handles special characters in input
    def test_handles_special_characters(self):
        # Given
        input_string = b"Hello World!@#$%^&*()_+"

        # When
        result = compute_sha_hash_value(input_string)

        # Then
        assert isinstance(result, str)
        assert len(result) == 64

    #  Supports non-ASCII characters in input
    def test_supports_non_ascii_characters(self):
        # Given
        input_string = "こんにちは".encode("utf-8")
        expected_hash = (
            "125aeadf27b0459b8760c13a3d80912dfa8a81a68261906f60d87f4a0268646c"
        )

        # When
        result = compute_sha_hash_value(input_string)

        # Then
        assert result == expected_hash

    #  Returns consistent hash value for same input and hash type
    def test_consistent_hash_value(self):
        # Given
        input_bytes = b"test"
        hash_type = HashType.SHA256

        # When
        hash_value_1 = compute_sha_hash_value(input_bytes, hash_type)
        hash_value_2 = compute_sha_hash_value(input_bytes, hash_type)

        # Then
        assert hash_value_1 == hash_value_2


class TestSaveOnDiskByFilepath:
    #  Saves a valid JSON dictionary to a valid file path.
    def test_save_valid_json_dict_to_valid_filepath(self, create_directory):
        # Given
        filepath = f"{create_directory}/data/test.json"
        data = {"name": "John", "age": 30}

        # When
        save_on_disk_by_filepath(filepath, data)

        # Then
        assert Path(filepath).exists()
        assert json.loads(Path(filepath).read_text()) == data

    #  Saves a valid JSON list to a valid file path.
    def test_save_valid_json_list_to_valid_filepath(self, create_directory):
        # Given
        filepath = f"{create_directory}/data/test.json"
        data = ["apple", "banana", "cherry"]

        # When
        save_on_disk_by_filepath(filepath, data)

        # Then
        assert Path(filepath).exists()
        assert json.loads(Path(filepath).read_text()) == data

    #  Saves a valid JSON string to a valid file path.
    def test_save_valid_json_string_to_valid_filepath(self, create_directory):
        # Given
        filepath = f"{create_directory}/data/test.json"
        data = '"Hello, World!"'

        # When
        save_on_disk_by_filepath(filepath, data)

        # Then
        assert Path(filepath).exists()
        assert json.loads(Path(filepath).read_text()) == data

    #  Saves a valid JSON integer to a valid file path.
    def test_save_valid_json_integer_to_valid_filepath(self, create_directory):
        # Given
        filepath = f"{create_directory}/data/test.json"
        data = 42

        # When
        save_on_disk_by_filepath(filepath, data)

        # Then
        assert Path(filepath).exists()
        assert json.loads(Path(filepath).read_text()) == data

    #  Saves a valid JSON float to a valid file path.
    def test_save_valid_json_float_to_valid_filepath(self, create_directory):
        # Given
        filepath = f"{create_directory}/data/test.json"
        data = 3.14

        # When
        save_on_disk_by_filepath(filepath, data)

        # Then
        assert Path(filepath).exists()
        assert json.loads(Path(filepath).read_text()) == data

    #  Raises InvalidDataError if data is not a valid dictionary or an empty dictionary.
    def test_raises_invalid_data_error_if_data_not_valid_dict(self, create_directory):
        # Given
        filepath = f"{create_directory}/data/test.json"
        data = {"invalid data"}

        # When, Then
        with pytest.raises(InvalidDataError):
            save_on_disk_by_filepath(filepath, data)


class TestCleanOutputDirectories:
    #  Function successfully cleans existing directories and creates new ones.
    def test_clean_existing_directories_and_create_new_ones(
        self, create_directory_hierarchy
    ):
        # Given
        directories = create_directory_hierarchy

        # When
        clean_output_directories(directories)

        # Then
        for directory in directories:
            assert os.path.isdir(directory)

    #  Function logs cleaning and creation of directories.
    #  Function handles empty input list.
    def test_handles_empty_input_list(self):
        # Given
        directories = []

        # When
        clean_output_directories(directories)

        # Then
        assert True  # No assertion needed, just checking for any errors

    #  Function handles input list with non-existent directories.
    def test_handles_non_existent_directories(self, create_directory):
        # Given
        directory = f"{create_directory}/non_existent_dir"

        # When
        clean_output_directories([directory])

        # Then
        assert os.path.isdir(directory)

    #  Function handles input list with directories containing files.
    def test_handles_directories_containing_files(self, create_directory):
        # Given
        directory = f"{create_directory}/dir_with_files"
        os.makedirs(directory)
        with open(f"{directory}/file.txt", "w") as f:
            f.write("test")

        # When
        clean_output_directories([directory])

        # Then
        assert os.path.isdir(directory)
        assert not os.path.isfile(f"{directory}/file.txt")

    #  Function handles input list with read-only directories.
    def test_handles_read_only_directories(self, create_directory):
        # Given
        directory = f"{create_directory}/read_only_dir"
        os.makedirs(directory)
        os.chmod(directory, 0o400)

        # When
        clean_output_directories([directory])

        # Then
        assert os.path.isdir(directory)

    #  Function handles input list with directories containing subdirectories.
    def test_handles_directories_containing_subdirectories(self, create_directory):
        # Given
        directory = f"{create_directory}/parent_dir"
        os.makedirs(f"{directory}/child_dir")

        # When
        clean_output_directories([directory])

        # Then
        assert os.path.isdir(directory)
        assert not os.path.isdir(f"{directory}/child_dir")

    #  Function handles input list with directories containing special characters.
    def test_handles_directories_containing_special_characters(self, create_directory):
        # Given
        directory = f"{create_directory}/dir_with_special_!@#$%^&*()_+[]|;:,.<>?~`"
        os.makedirs(directory)

        # When
        clean_output_directories([directory])

        # Then
        assert os.path.isdir(directory)

    #  Function handles input list with duplicate directories.
    def test_handles_duplicate_directories(self, create_directory):
        # Given
        directory = f"{create_directory}/duplicate_dir"

        # When
        clean_output_directories([directory, directory])

        # Then
        assert os.path.isdir(directory)



class TestGetObjectId:

    #  Returns a valid URL when all parameters are provided.
    def test_returns_valid_url_with_all_parameters(self):
        # Given
        base_url = "https://example.com"
        folder = "data"
        uuid = UUID("123e4567-e89b-12d3-a456-426614174000")
        filename = "data.json"

        # When
        result = generate_object_id(base_url, folder, uuid, filename)

        # Then
        assert result == "https://example.com/data/123e4567-e89b-12d3-a456-426614174000/data.json"

    #  Returns a valid URL when only the required parameters are provided.
    def test_returns_valid_url_with_required_parameters(self):
        # Given
        base_url = "https://example.com"
        folder = "data"
        uuid = UUID("123e4567-e89b-12d3-a456-426614174000")

        # When
        result = generate_object_id(base_url, folder, uuid)

        # Then
        assert result == "https://example.com/data/123e4567-e89b-12d3-a456-426614174000/data.json"

    #  Returns a valid URL when the filename parameter is not provided.
    def test_returns_valid_url_without_filename_parameter(self):
        # Given
        base_url = "https://example.com"
        folder = "data"
        uuid = UUID("123e4567-e89b-12d3-a456-426614174000")

        # When
        result = generate_object_id(base_url, folder, uuid)

        # Then
        assert result == "https://example.com/data/123e4567-e89b-12d3-a456-426614174000/data.json"

    #  Returns a valid URL when the base_url parameter ends with a forward slash.
    def test_returns_valid_url_with_base_url_ending_with_forward_slash(self):
        # Given
        base_url = "https://example.com/"
        folder = "data"
        uuid = UUID("123e4567-e89b-12d3-a456-426614174000")
        filename = "data.json"

        # When
        result = generate_object_id(base_url, folder, uuid, filename)

        # Then
        assert result == "https://example.com/data/123e4567-e89b-12d3-a456-426614174000/data.json"

    #  Returns a valid URL when the folder parameter ends with a forward slash.
    def test_returns_valid_url_with_folder_ending_with_forward_slash(self):
        # Given
        base_url = "https://example.com"
        folder = "data/"
        uuid = UUID("123e4567-e89b-12d3-a456-426614174000")
        filename = "data.json"

        # When
        result = generate_object_id(base_url, folder, uuid, filename)

        # Then
        assert result == "https://example.com/data/123e4567-e89b-12d3-a456-426614174000/data.json"

    #  Raises a ValueError when the base_url parameter is None.
    def test_raises_value_error_with_none_base_url(self):
        # Given
        base_url = None
        folder = "data"
        uuid = UUID("123e4567-e89b-12d3-a456-426614174000")
        filename = "data.json"

        # When/Then
        with pytest.raises(ValueError):
            generate_object_id(base_url, folder, uuid, filename)

    #  Raises a ValueError when the base_url parameter is an empty string.
    def test_raises_value_error_with_empty_base_url(self):
        # Given
        base_url = ""
        folder = "data"
        uuid = UUID("123e4567-e89b-12d3-a456-426614174000")
        filename = "data.json"

        # When/Then
        with pytest.raises(ValueError):
            generate_object_id(base_url, folder, uuid, filename)

    #  Raises a ValueError when the folder parameter is None.
    def test_raises_value_error_with_none_folder(self):
        # Given
        base_url = "https://example.com"
        folder = None
        uuid = UUID("123e4567-e89b-12d3-a456-426614174000")
        filename = "data.json"

        # When/Then
        with pytest.raises(ValueError):
            generate_object_id(base_url, folder, uuid, filename)

    #  Raises a ValueError when the folder parameter is an empty string.
    def test_raises_value_error_with_empty_folder(self):
        # Given
        base_url = "https://example.com"
        folder = ""
        uuid = UUID("123e4567-e89b-12d3-a456-426614174000")
        filename = "data.json"

        # When/Then
        with pytest.raises(ValueError):
            generate_object_id(base_url, folder, uuid, filename)

    #  Raises a ValueError when the uuid parameter is None.
    def test_raises_value_error_with_none_uuid(self):
        # Given
        base_url = "https://example.com"
        folder = "data"
        uuid = None
        filename = "data.json"

        # When/Then
        with pytest.raises(ValueError):
            generate_object_id(base_url, folder, uuid, filename)


class TestIsKnownOntology:

    #  Returns True when given a list of known ontology types
    def test_returns_true_when_given_list_of_known_ontology_types(self):
        # Given
        ontology_types = ["Location", "ServiceOffering", "ComplianceCriterion"]

        # When
        result = is_known_ontology(ontology_types)

        # Then
        assert result is True

    #  Returns False when given an empty list
    def test_returns_false_when_given_empty_list(self):
        # Given
        ontology_types = []

        # When
        result = is_known_ontology(ontology_types)

        # Then
        assert result is False

    #  Returns False when given a non-list input
    def test_returns_false_when_given_non_list_input(self):
        # Given
        ontology_types = {"Location"}

        # When
        result = is_known_ontology(ontology_types)

        # Then
        assert result is False

    #  Returns False when given a list with non-string values
    def test_returns_false_when_given_list_with_non_string_values(self):
        # Given
        ontology_types = ["Location", 123, "ServiceOffering"]

        # When
        result = is_known_ontology(ontology_types)

        # Then
        assert result is False

    #  Returns True when given a list with a single known ontology type
    def test_returns_true_when_given_list_with_single_known_ontology_type(self):
        # Given
        ontology_types = ["Location"]

        # When
        result = is_known_ontology(ontology_types)

        # Then
        assert result is True

    #  Returns False when given None as input
    def test_returns_false_when_given_none_as_input(self):
        # Given
        ontology_types = None

        # When
        result = is_known_ontology(ontology_types)

        # Then
        assert result is False

    #  Returns False when ALLOWED_OBJECT_ONTOLOGY is empty
    def test_returns_false_when_allowed_object_ontology_is_empty(self):
        # Given
        ontology_types = ["Location", "ServiceOffering"]
        ALLOWED_OBJECT_ONTOLOGY.clear()

        # When
        result = is_known_ontology(ontology_types)

        # Then
        assert result is False

    #  Returns False when ALLOWED_OBJECT_ONTOLOGY is not a dictionary
    def test_returns_false_when_allowed_object_ontology_is_not_dictionary(self):
        # Given
        ontology_types = ["Location", "ServiceOffering"]
        ALLOWED_OBJECT_ONTOLOGY = ["Location", "ServiceOffering"]

        # When
        result = is_known_ontology(ontology_types)

        # Then
        assert result is False

    #  Returns False when ALLOWED_OBJECT_ONTOLOGY values are not strings
    def test_returns_false_when_allowed_object_ontology_values_are_not_strings(self):
        # Given
        ontology_types = ["Location", "ServiceOffering"]
        ALLOWED_OBJECT_ONTOLOGY = {
            "location": "Location",
            "service_offering": 123,
            "compliance_criterion": "ComplianceCriterion"
        }

        # When
        result = is_known_ontology(ontology_types)

        # Then
        assert result is False

    #  Returns False when ALLOWED_OBJECT_ONTOLOGY values are not unique
    def test_returns_false_when_allowed_object_ontology_values_are_not_unique(self):
        # Given
        ontology_types = ["Location", "ServiceOffering"]
        ALLOWED_OBJECT_ONTOLOGY = {
            "location": "Location",
            "service_offering": "Location",
            "compliance_criterion": "ComplianceCriterion"
        }

        # When
        result = is_known_ontology(ontology_types)

        # Then
        assert result is False

class TestExtractFirstObjectType:

    #  Extracts the type of the first object from a list of object types.
    def test_extract_first_object_type_extract_type_from_list(self):
        # Given
        object_types = ["Person", "Organization", "Event"]

        # When
        result = extract_first_object_type(object_types)

        # Then
        assert result == "Person"

    #  Returns the type of the first object.
    def test_extract_first_object_type_returns_type_of_first_object(self):
        # Given
        object_types = ["Person", "Organization", "Event"]

        # When
        result = extract_first_object_type(object_types)

        # Then
        assert result == "Person"

    #  Handles valid input format.
    def test_extract_first_object_type_handles_valid_input_format(self):
        # Given
        object_types = ["Person", "Organization", "Event"]

        # When
        result = extract_first_object_type(object_types)

        # Then
        assert result == "Person"

    #  Handles empty input list.
    def test_extract_first_object_type_handles_empty_input_list(self):
        # Given
        object_types = []

        # When
        # Then
        with pytest.raises(ValueError):
            extract_first_object_type(object_types)

    #  Handles input list with one element.
    def test_extract_first_object_type_handles_input_list_with_one_element(self):
        # Given
        object_types = ["Person"]

        # When
        result = extract_first_object_type(object_types)

        # Then
        assert result == "Person"

    #  Handles input list with multiple elements.
    def test_extract_first_object_type_handles_input_list_with_multiple_elements(self):
        # Given
        object_types = ["Person", "Organization", "Event"]

        # When
        result = extract_first_object_type(object_types)

        # Then
        assert result == "Person"

    #  Handles input list with non-string elements.
    def test_extract_first_object_type_handles_input_list_with_non_string_elements(self):
        # Given
        object_types = ["Person", 123, True]

        # When
        result = extract_first_object_type(object_types)

        # Then
        assert result == "Person"

    #  Handles input list with empty string elements.
    def test_extract_first_object_type_handles_input_list_with_empty_string_elements(self):
        # Given
        object_types = ["Person", "", "Event"]

        # When
        result = extract_first_object_type(object_types)

        # Then
        assert result == "Person"

    #  Handles input list with whitespace-only string elements.
    def test_extract_first_object_type_handles_input_list_with_whitespace_only_string_elements(self):
        # Given
        object_types = ["Person", "   ", "Event"]

        # When
        result = extract_first_object_type(object_types)

        # Then
        assert result == "Person"

    #  Handles input list with non-alphanumeric string elements.
    def test_extract_first_object_type_handles_input_list_with_non_alphanumeric_string_elements(self):
        # Given
        object_types = ["Person", "!@#$%", "Event"]

        # When
        result = extract_first_object_type(object_types)

        # Then
        assert result == "Person"

class TestCheckObjectIdValidity:

    #  Valid object ID with known object-type, uuid and right filename
    @pytest.mark.parametrize("object_type", VALID_OBJECT_DIRECTORIES)
    def test_valid_object_id(self, object_type):
        # Given
        object_id = f"http://ovhcloud.provider.example.com/{object_type}/67cc61fa-5d7f-4e8c-b73a-39de730cd281/data.json"
        # When
        extract_information_from_object_id(object_id)

        # Then
        # No exception should be raised


    def test_invalid_object_id_with_unknown_object_type(self):
        # Given
        object_id = 'http://example.com/object-type/67cc61fa-5d7f-4e8c-b73a-39de730cd281/data.json'

        # When, Then
        with pytest.raises(ValueError) as e:
            extract_information_from_object_id(object_id)
        assert str(
            e.value) == "The object-type 'object-type' in the object ID http://example.com/object-type/67cc61fa-5d7f-4e8c-b73a-39de730cd281/data.json is unknown."

    def test_invalid_object_id_with_no_uuid(self):
        # Given
        object_id = 'http://example.com/location/data.json'

        # When, Then
        with pytest.raises(ValueError) as e:
            extract_information_from_object_id(object_id)
        assert str(
            e.value) == "http://example.com/location/data.json is invalid. It should be <scheme>://<domain name>/<object type>/<identifier>/data.json"


    def test_invalid_object_id_with_missing_filename(self):
        # Given
        object_id = 'http://example.com/location/cd050df4-c809-45dd-aafe-39a093c6a777'
        # When, Then
        with pytest.raises(ValueError) as e:
            extract_information_from_object_id(object_id)
        assert str(
            e.value) == f"{object_id} is invalid. It should be <scheme>://<domain name>/<object type>/<identifier>/data.json"


    @pytest.mark.parametrize("object_id, filename", [
        ('http://example.com/location/cd050df4-c809-45dd-aafe-39a093c6a777/', ''),
        ('http://example.com/location/cd050df4-c809-45dd-aafe-39a093c6a777/?filename=data.json', ''),
        ('http://example.com/location/cd050df4-c809-45dd-aafe-39a093c6a777/#data.json', ''),
        ('http://example.com/location/cd050df4-c809-45dd-aafe-39a093c6a777/filename', 'filename'),
    ])
    def test_invalid_object_id_with_invalid_filename(self, object_id, filename):
        # Given

        # When, Then
        with pytest.raises(ValueError) as e:
            extract_information_from_object_id(object_id)
        assert str(
            e.value) == f"The filename '{filename}' in the path of {object_id} is invalid."


    def test_invalid_object_id_with_did_web(self):
        # Given
        object_id = 'did:web:ovhcloud.provider.example.com:participant:68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/gaiax-terms-and-conditions.json'
        # When, Then
        with pytest.raises(ValueError) as e:
            extract_information_from_object_id(object_id)
        assert str(
            e.value) == f"{object_id} is invalid. It should be <scheme>://<domain name>/<object type>/<uuid-4>/data.json"
