import http
import json
import os
from pathlib import Path

from agent.config import settings


class TestHealthcheck:

    #  Returns a JSON object with status 'OK' and participant name.
    def test_returns_json_object_with_status_ok_and_participant_name(self, client):
        # Given
        # When
        response = client.get('/healthcheck')
        data = response.get_json()

        # Then
        assert response.status_code == http.HTTPStatus.OK
        assert data['status'] == 'OK'
        assert data['participant_name'] == settings.participant_name

    #  The function is called with invalid parameters.
    def test_function_called_with_invalid_parameters(self, client):
        # Given
        # When
        response = client.get('/healthcheck/invalid')

        # Then
        assert response.status_code == 404

class TestExposeDirectory:

    #  Returns a file from the specified directory when given a valid path.
    def test_returns_file_from_specified_directory_when_given_valid_path(self, client):
        # Given
        # When
        response = client.get("/txt/foo.txt")

        # Then
        assert response.status_code == 200
        assert response.data == b"foo"
        assert response.content_type == "text/plain; charset=utf-8"
        assert response.headers["Access-Control-Allow-Origin"] == "*"
        assert response.headers["Content-Disposition"] == "inline; filename=foo.txt"

    def test_returns_directory_when_given_valid_path(self, client):
        # Given
        # When
        response = client.get("/txt")

        # Then
        assert response.status_code == 200
        assert "<a href=\"/txt/..\">Parent folder</a></td>" in response.text
        assert "<a href=\"/txt/foo.txt\">foo.txt</a></td>" in response.text
        assert "<a href=\"/txt/bar.txt\">bar.txt</a></td>" in response.text
        assert "<a href=\"/txt/baz.txt\">baz.txt</a></td>" in response.text
        assert response.content_type == "text/html; charset=utf-8"
        assert response.headers["Access-Control-Allow-Origin"] == "*"


    #  Returns a 404 error when given an invalid path.
    def test_returns_404_error_when_given_invalid_path(self, client):
        # Given
        # When
        response = client.get("/invalid/path")

        # Then
        assert response.status_code == 404

    #  Returns a 404 error when given a path with directory traversal.
    def test_returns_404_error_when_given_path_with_directory_traversal(self, client):
        # Given
        # When
        response = client.get("/txt/../file.txt")

        # Then
        assert response.status_code == 404

    #  Returns a 404 error when given a path with null bytes.
    def test_returns_404_error_when_given_path_with_null_bytes(self, client):
        # Given
        # When
        response = client.get("/txt/\x00file.txt")

        # Then
        assert response.status_code == 404

class TestSecuredRoute:

    #  The function returns a JSON response with a test message when a valid API key is provided.
    def test_valid_api_key_provided(self, client):
        # Given
        valid_api_key = os.getenv("API_KEY_AUTHORIZED")

        # When
        response = client.get('/test-api-key', headers={'X-API-KEY': valid_api_key})

        # Then
        assert response.status_code == http.HTTPStatus.OK
        assert response.json == {'msg': 'It works!'}

    #  The function returns an HTTP status code of 401 when an invalid API key is provided.
    def test_invalid_api_key_provided_status_code(self, client):
        # Given
        invalid_api_key = 'invalid_api_key'

        # When
        response = client.get('/test-api-key', headers={'X-API-KEY': invalid_api_key})

        # Then
        assert response.status_code == http.HTTPStatus.UNAUTHORIZED
        assert response.json == {'message': 'api key is invalid'}

class TestBootstrapProvider:

    #  The function is called with a valid API key and TLS certificate path.
    def test_bootstrap_provider_with_valid_api_key(self, client):
        # Given
        api_key = os.getenv("API_KEY_AUTHORIZED")

        # When
        response = client.post(
            "/api/bootstrap-provider",
            headers={"X-API-KEY": api_key}
        )

        # Then
        assert response.status_code == http.HTTPStatus.CREATED
        assert response.json == {"id":"did:web:ovhcloud.provider.dev.gaiax.ovh","msg":"DID Document successfully created"}

        root_directory = Path(settings.public_folder_to_expose)
        subfolders = [f.name for f in os.scandir(root_directory) if f.is_dir()]
        assert ".well-known" in subfolders

    def test_bootstrap_provider_with_invalid_api_key(self, client):
        # Given
        api_key = "invalid-api-key"

        # When
        response = client.post(
            "/api/bootstrap-provider",
            headers={"X-API-KEY": api_key}
        )

        # Then
        assert response.status_code == http.HTTPStatus.UNAUTHORIZED
        assert response.json == {'message': 'api key is invalid'}

        root_directory = Path(settings.public_folder_to_expose)
        subfolders = [f.name for f in os.scandir(root_directory) if f.is_dir()]
        assert ".well-known" not in subfolders
        assert "participant" not in subfolders

class TestCleanData:

    #  The function successfully deletes the contents of the specified directory.
    def test_successfully_deletes_contents_of_directory(self, client):
        # Given
        valid_api_key = os.getenv("API_KEY_AUTHORIZED")

        # When
        response = client.delete("/api/clean-data", headers={"X-API-KEY": valid_api_key})

        # Then
        assert response.status_code == http.HTTPStatus.NO_CONTENT
        assert not os.listdir(settings.public_folder_to_expose)

    #  The function is called with an incorrect HTTP method, and the function returns an error message and HTTP status code 405.
    def test_called_with_incorrect_http_method_returns_error_message_and_status_code_405(self, client):
        # Given
        valid_api_key = os.getenv("API_KEY_AUTHORIZED")

        # When
        response = client.get("/api/clean-data", headers={"X-API-KEY": valid_api_key})

        # Then
        assert response.status_code == http.HTTPStatus.NOT_FOUND
        assert response.json is None

    #  The function is called without a valid API key, and the function returns an error message and HTTP status code 401.
    def test_called_without_valid_api_key_returns_error_message_and_status_code_401(self, client):
        # Given
        # When
        response = client.delete("/api/clean-data")

        # Then
        assert response.status_code == http.HTTPStatus.UNAUTHORIZED
        assert response.json == {"message": "a valid api key is missing"}

    def test_called_with_invalid_api_key_returns_error_message_and_status_code_401(self, client):
        # Given
        api_key = "invalid-api-key"

        # When
        response = client.delete("/api/clean-data", headers={"X-API-KEY": api_key})


        # Then
        assert response.status_code == http.HTTPStatus.UNAUTHORIZED
        assert response.json == {'message': 'api key is invalid'}

class TestStoreObject:

    #  Store a valid json-ld object of allowed type, return 200 OK
    def test_store_valid_json_ld_object(self, client):
        # Given
        json_data = {
            "@context": "https://www.w3.org/ns/did/v1",
            "id": "http://example.com/legal-participant-json/0d347d02-a25c-493a-a40d-78b423ee947a/data.json",
            "type": "LegalParticipant",
            "name": "Example Participant",
        }

        # When
        response = client.post(
            "/api/store_object/legal-participant",
            json={"objectjson": json_data},
            headers={"x-api-key": os.getenv("API_KEY_AUTHORIZED")},
        )

        # Then
        assert response.status_code == http.HTTPStatus.CREATED
        assert response.json == {'id': 'http://example.com/legal-participant-json/0d347d02-a25c-493a-a40d-78b423ee947a/data.json', 'msg': 'Object sucessfully stored'}

    #  Store a valid json-ld file of allowed type, return 200 OK
    # def test_store_valid_json_ld_file(self, client, create_directory_hierarchy):
    #     # Given
    #     json_data = {
    #         "@context": "https://www.w3.org/ns/did/v1",
    #         "id": "did:example:123:participant/123/legal-participant.json",
    #         "type": "LegalParticipant",
    #         "name": "Example Participant",
    #     }
    #
    #     parent_dir = Path(create_directory_hierarchy[1])
    #     file = Path(parent_dir, "example.json")
    #     with open(file, 'w') as f:
    #         json.dump(json_data, f)
    #
    #     # When
    #     response = client.post(
    #         "/api/store_object/legal-participant",
    #         data={"objectfile": (file, "example.json")},
    #         content_type="multipart/form-data",
    #         headers={"x-api-key": os.getenv("API_KEY_AUTHORIZED")},
    #     )
    #
    #     # Then
    #     assert response.status_code == http.HTTPStatus.CREATED
    #     assert response.json == {"msg": "OK"}

    #  Store a valid json-ld object of type 'legal-participant' and ontology type 'gx:LegalParticipant', return 200 OK
    def test_store_valid_json_ld_object_legal_participant_gx_legal_participant(self, client):
        # Given
        json_data = {
            "@context": "https://www.w3.org/ns/did/v1",
            "id": "http://example.com/legal-participant-json/0d347d02-a25c-493a-a40d-78b423ee947a/data.json",
            "type": "gx:LegalParticipant",
            "name": "Example Participant",
        }

        # When
        response = client.post(
            "/api/store_object/legal-participant",
            json={"objectjson": json_data},
            headers={"x-api-key": os.getenv("API_KEY_AUTHORIZED")},
        )

        # Then
        assert response.status_code == http.HTTPStatus.CREATED
        assert response.json == {'id': 'http://example.com/legal-participant-json/0d347d02-a25c-493a-a40d-78b423ee947a/data.json',
 'msg': 'Object sucessfully stored'}

    #  Store a valid json-ld object of type 'legal-participant' and ontology type 'gx:GaiaXTermsAndConditions', return 200 OK
    def test_store_valid_json_ld_object_legal_participant_gx_gaiax_terms_and_conditions(self, client):
        # Given
        json_data = {
            "@context": "https://www.w3.org/ns/did/v1",
            "id": "http://example.com/legal-participant-json/0d347d02-a25c-493a-a40d-78b423ee947a/data.json",
            "type": "LegalParticipant",
            "name": "Example Participant",
        }

        # When
        response = client.post(
            "/api/store_object/legal-participant",
            json={"objectjson": json_data},
            headers={"x-api-key": os.getenv("API_KEY_AUTHORIZED")},
        )

        # Then
        assert response.status_code == http.HTTPStatus.CREATED
        assert response.json == {'id': 'http://example.com/legal-participant-json/0d347d02-a25c-493a-a40d-78b423ee947a/data.json',
 'msg': 'Object sucessfully stored'}

    #  Store a valid json-ld object of type 'legal-participant' and ontology type not allowed, return 422 UNPROCESSABLE_ENTITY
    def test_store_valid_json_ld_object_legal_participant_not_allowed(self, client):
        # Given
        json_data = {
            "@context": "https://www.w3.org/ns/did/v1",
            "id": "http://example.com/legal-participant-json/0d347d02-a25c-493a-a40d-78b423ee947a/data.json",
            "type": "LegalParticipant",
            "name": "Example Participant",
        }

        # When
        response = client.post(
            "/api/store_object/invalid-type",
            json={"objectjson": json_data},
            headers={"x-api-key": os.getenv("API_KEY_AUTHORIZED")},
        )

        # Then
        assert response.status_code == http.HTTPStatus.BAD_REQUEST

    #  Store an empty json-ld object, return 422 UNPROCESSABLE_ENTITY
    def test_store_empty_json_ld_object(self, client):
        # Given
        json_data = {}

        # When
        response = client.post(
            "/api/store_object/legal-participant",
            json={"objectjson": json_data},
            headers={"x-api-key": os.getenv("API_KEY_AUTHORIZED")},
        )

        # Then
        assert response.status_code == http.HTTPStatus.BAD_REQUEST
