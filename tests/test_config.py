import hashlib
import os
from socket import socket, AF_INET, SOCK_STREAM

import pytest

from agent.config import Settings, LogLevel, Environment


class TestSettings:

    #  The class can be instantiated with default values.
    def test_instantiation_with_default_values(self):
        # Given
        os.environ["API_KEY_AUTHORIZED"] = ""
        os.environ["PARTICIPANT_NAME"] = "ovhcloud"
        os.environ["PARENT_DOMAIN"] = "provider.dev.gaiax.ovh"

        # When
        settings = Settings()

        # Then
        assert settings.log_level == LogLevel.debug
        assert settings.environment == Environment.dev
        assert settings.api_key_authorized == "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"
        assert settings.api_port_exposed == 5001
        assert settings.participant_name == "ovhcloud"
        assert settings.parent_domain == "provider.dev.gaiax.ovh"

    #  The class can be instantiated with custom values.
    def test_instantiation_with_custom_values(self):
        # Given
        log_level = LogLevel.warning
        environment = Environment.prod
        api_key_authorized = "custom_api_key"
        api_port_exposed = 8080
        participant_name = "custom_participant"
        parent_domain = "custom_domain"

        expected_hash = hashlib.sha256(api_key_authorized.encode()).hexdigest()

        # When
        settings = Settings(
            log_level=log_level,
            environment=environment,
            api_key_authorized=api_key_authorized,
            api_port_exposed=api_port_exposed,
            participant_name=participant_name,
            parent_domain=parent_domain,
        )
        # Then
        assert settings.log_level == log_level
        assert settings.environment == environment
        assert settings.api_key_authorized == expected_hash
        assert settings.api_port_exposed == api_port_exposed
        assert settings.participant_name == participant_name
        assert settings.parent_domain == parent_domain

    #  The class can validate a valid API key.
    def test_valid_api_key_validation(self):
        # Given
        api_key = "valid_api_key"
        expected_hash = hashlib.sha256(api_key.encode()).hexdigest()
        # When
        settings = Settings(api_key_authorized=api_key)
        # Then
        assert settings.api_key_authorized == expected_hash

    #  The class can validate a valid API port.
    def test_valid_api_port_validation(self):
        # Given
        api_port = 8080
        # When
        settings = Settings(api_port_exposed=api_port)
        # Then
        assert settings.api_port_exposed == api_port

    #  The class can generate a valid DID URL.
    def test_valid_did_url_generation(self):
        # Given
        participant_name = "test_participant"
        parent_domain = "test_domain"
        expected_did_url = f"did:web:{participant_name}.{parent_domain}"
        # When
        settings = Settings(participant_name=participant_name, parent_domain=parent_domain)
        # Then
        assert settings.did_url == expected_did_url

    #  The class raises an error if API key is not provided.
    def test_error_raised_if_api_key_not_provided(self):
        # Given
        del os.environ["API_KEY_AUTHORIZED"]
        # When, Then
        with pytest.raises(ValueError):
            Settings()

    #  The class raises an error if participant name is not provided.
    def test_error_raised_if_participant_name_not_provided(self):
        # Given
        # When, Then
        with pytest.raises(ValueError):
            Settings(api_key_authorized="valid_api_key", participant_name=None)

    #  The class raises an error if parent domain is not provided.
    def test_error_raised_if_parent_domain_not_provided(self):
        # Given
        # When, Then
        with pytest.raises(ValueError):
            Settings(api_key_authorized="valid_api_key", participant_name="test_participant", parent_domain_name=None)

    #  The class can be used to set log level.
    def test_set_log_level(self):
        # Given
        settings = Settings(api_key_authorized="valid_api_key")
        # When
        settings.log_level = LogLevel.warning
        # Then
        assert settings.log_level == LogLevel.warning

    #  The class can be used to set environment.
    def test_set_environment(self):
        # Given
        settings = Settings(api_key_authorized="valid_api_key")
        # When
        settings.environment = Environment.prod
        # Then
        assert settings.environment == Environment.prod
