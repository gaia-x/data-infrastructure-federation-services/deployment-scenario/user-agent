import os
from pathlib import Path

from agent.config import settings
from agent.use_cases.storage import clean_public_folder


class TestCleanPublicFolder:

    #  Should return True if the public folder is successfully cleaned
    def test_return_true_if_public_folder_successfully_cleaned(self, create_directory_hierarchy):
        # Given
        txt_parent, _ = create_directory_hierarchy
        settings.public_folder_to_expose = Path(txt_parent).parent

        # When
        result = clean_public_folder()

        # Then
        assert result is True

    #  Should return False if the public folder is empty
    def test_return_false_if_public_folder_empty(self, tmp_path_factory):
        # Given
        settings.public_folder_to_expose = tmp_path_factory.mktemp("empty")

        # When
        result = clean_public_folder()

        # Then
        assert result is False

    #  Should return False if the public folder does not exist
    def test_return_false_if_public_folder_does_not_exist(self, create_directory_hierarchy):
        # Given
        settings.public_folder_to_expose = "/path/to/nonexistent/folder"

        # When
        result = clean_public_folder()

        # Then
        assert result is False

    #  Should remove all files and directories inside the public folder
    def test_remove_all_files_and_directories_inside_public_folder(self, create_directory_hierarchy):
        # Given
        txt_parent, _ = create_directory_hierarchy
        settings.public_folder_to_expose = Path(txt_parent).parent

        # When
        result = clean_public_folder()

        # Then
        assert result is True
        assert os.listdir(settings.public_folder_to_expose) == []
