import json
import os
from pathlib import Path
import pytest
from agent.use_cases.did_document import DIDDocumentGenerator


class TestDidDocumentGeneratorInitializer:

    #  Initializes the object with valid arguments.
    def test_initializes_with_valid_arguments(self, create_cert_and_public_files):
        # Given
        tls_cert_path, out_folder = create_cert_and_public_files
        server_name = "example.com"

        # When
        obj = DIDDocumentGenerator(tls_cert_path, out_folder, server_name)

        # Then
        assert obj.tls_cert_path == tls_cert_path
        assert obj.out_folder == out_folder
        assert obj.server_name == server_name


    #  Raises a 'FileNotFoundError' when the 'tls_cert_path' does not exist.
    def test_raises_FileNotFoundError_when_tls_cert_path_does_not_exist(self, create_cert_and_public_files):
        # Given
        _, out_folder = create_cert_and_public_files

        tls_cert_path = "nonexistent_cert.pem"
        server_name = "example.com"

        # When, Then
        with pytest.raises(FileNotFoundError):
            DIDDocumentGenerator(tls_cert_path, out_folder, server_name)

    #  Raises a 'ValueError' when the 'out_folder' does not exist and is not a directory.
    def test_raises_ValueError_when_out_folder_does_not_exist(self, create_cert_and_public_files):
        # Given
        tls_cert_path, _ = create_cert_and_public_files
        out_folder = "nonexistent_folder"
        server_name = "example.com"

        # When, Then
        with pytest.raises(ValueError):
            DIDDocumentGenerator(tls_cert_path, out_folder, server_name)

class TestDidDocumentGeneratorRetrieveCertChain:

    #  Creates a '.well-known' directory if it doesn't exist
    def test_check_well_known_directory(self, create_cert_and_public_files):
        # Given
        tls_cert_path, out_folder = create_cert_and_public_files
        server_name = "example.com"
        generator = DIDDocumentGenerator(tls_cert_path, out_folder, server_name)

        # When
        generator._retrieve_cert_chain()

        # Then
        well_known_dir = Path(out_folder, ".well-known")
        assert well_known_dir.exists()
        assert well_known_dir.is_dir()

        cert_chain_path = Path(well_known_dir, "chain.pem")
        assert cert_chain_path.exists()
        assert cert_chain_path.is_file()

class TestDidDocumentGeneratorGenerateId:

    #  Generates a unique identifier by combining the DID root and the given id name.
    def test_generate_id_unique_identifier(self):
        # Given
        did_root = "did:example:123"
        id_name = "test"

        # When
        result = DIDDocumentGenerator._generate_id(did_root, id_name)

        # Then
        assert isinstance(result, str)
        assert result == "did:example:123#test"

    #  Returns the generated unique identifier.
    def test_generate_id_return_identifier(self):
        # Given
        did_root = "did:example:123"
        id_name = "test"

        # When
        result = DIDDocumentGenerator._generate_id(did_root, id_name)

        # Then
        assert isinstance(result, str)
        assert result == "did:example:123#test"

    #  Returns the DID root if id_name is empty.
    def test_generate_id_return_did_root(self):
        # Given
        did_root = "did:example:123"
        id_name = ""

        # When
        result = DIDDocumentGenerator._generate_id(did_root, id_name)

        # Then
        assert isinstance(result, str)
        assert result == "did:example:123"

    #  Accepts only string arguments for did_root and id_name.
    def test_generate_id_accepts_string_arguments(self):
        # Given
        did_root = "did:example:123"
        id_name = "test"

        # When
        result = DIDDocumentGenerator._generate_id(did_root, id_name)

        # Then
        assert isinstance(result, str)

    #  Allows only alphanumeric characters for id_name.
    def test_generate_id_allows_alphanumeric_characters(self):
        # Given
        did_root = "did:example:123"
        id_name = "test123"

        # When
        result = DIDDocumentGenerator._generate_id(did_root, id_name)

        # Then
        assert isinstance(result, str)
        assert result == "did:example:123#test123"

    #  Raises TypeError if did_root is not a string.
    def test_generate_id_raises_type_error_did_root_not_string(self):
        # Given
        did_root = 123
        id_name = "test"

        # When/Then
        with pytest.raises(TypeError):
            DIDDocumentGenerator._generate_id(did_root, id_name)

    #  Raises ValueError if did_root is empty or does not start with 'did:'.
    def test_generate_id_raises_value_error_invalid_did_root(self):
        # Given
        did_root = ""
        id_name = "test"

        # When/Then
        with pytest.raises(ValueError):
            DIDDocumentGenerator._generate_id(did_root, id_name)

        # Given
        did_root = "example:123"
        id_name = "test"

        # When/Then
        with pytest.raises(ValueError):
            DIDDocumentGenerator._generate_id(did_root, id_name)

    #  Raises TypeError if id_name is not a string.
    def test_generate_id_raises_type_error_id_name_not_string(self):
        # Given
        did_root = "did:example:123"
        id_name = 123

        # When/Then
        with pytest.raises(TypeError):
            DIDDocumentGenerator._generate_id(did_root, id_name)

class TestDidDocumentGeneratorCreateDidDocument:

    #  Generates a DID document with the expected fields and values.
    def test_generate_did_document_with_expected_fields_and_values(self, create_cert_and_public_files):
        # Given
        tls_cert_path, out_folder = create_cert_and_public_files
        server_name = "example.com"
        generator = DIDDocumentGenerator(tls_cert_path, out_folder, server_name)

        with open(tls_cert_path, "r") as cert_chain_file:
            certificate_chain = cert_chain_file.read()
        # When
        generator._create_did_document(certificate_chain=certificate_chain)

        # Then
        out_did_document_path = Path(out_folder, ".well-known","did.json")
        assert out_did_document_path.exists()

        with open(out_did_document_path, "r") as f:
            did_document = json.load(f)

        assert "@context" in did_document
        context = did_document["@context"]
        assert isinstance(context, list)
        assert all(current_context in context for current_context in [
            "https://www.w3.org/ns/did/v1",
            "https://w3id.org/security/suites/jws-2020/v1",
        ])

        assert "id" in did_document
        assert "verificationMethod" in did_document
        assert "authentication" in did_document
        assert "assertionMethod" in did_document
        assert "service" in did_document

    #  Builds a JWK in RFC 7797 format with the expected fields and values.
    def test_build_jwk_with_expected_fields_and_values(self, create_cert_and_public_files):
        # Given
        tls_cert_path, out_folder = create_cert_and_public_files
        server_name = "example.com"
        generator = DIDDocumentGenerator(tls_cert_path, out_folder, server_name)

        with open(tls_cert_path, "r") as cert_chain_file:
            certificate_chain = cert_chain_file.read()
        # When
        generator._create_did_document(certificate_chain=certificate_chain)

        # Then
        out_did_document_path = Path(out_folder, ".well-known","did.json")
        assert out_did_document_path.exists()
        with open(out_did_document_path, "r") as f:
            did_document = json.load(f)

        assert "verificationMethod" in did_document
        assert isinstance(did_document["verificationMethod"], list)
        assert len(did_document["verificationMethod"]) > 0

        verification_method = did_document["verificationMethod"][0]
        assert "alg" in verification_method["publicKeyJwk"]
        assert "x5u" in verification_method["publicKeyJwk"]

    #  Uses the correct verification method ID, authentication ID, and assertion method ID.
    def test_uses_correct_verification_authentication_and_assertion_ids(self, create_cert_and_public_files):
        # Given
        tls_cert_path, out_folder = create_cert_and_public_files
        server_name = "example.com"
        generator = DIDDocumentGenerator(tls_cert_path, out_folder, server_name)

        with open(tls_cert_path, "r") as cert_chain_file:
            certificate_chain = cert_chain_file.read()
        # When
        generator._create_did_document(certificate_chain=certificate_chain)

        # Then
        out_did_document_path = Path(out_folder, ".well-known","did.json")
        assert out_did_document_path.exists()
        with open(out_did_document_path, "r") as f:
            did_document = json.load(f)

        assert "verificationMethod" in did_document
        assert isinstance(did_document["verificationMethod"], list)
        assert len(did_document["verificationMethod"]) > 0

        verification_method = did_document["verificationMethod"][0]
        assert "id" in verification_method
        assert "alg" in verification_method["publicKeyJwk"]
        assert "x5u" in verification_method["publicKeyJwk"]
        verification_method_id = verification_method["id"]
        assert verification_method_id.startswith("did:web:")
        assert verification_method["publicKeyJwk"]["x5u"].startswith("https://example.com/.well-known/chain.pem")

        assert "authentication" in did_document
        assert isinstance(did_document["authentication"], list)
        assert len(did_document["authentication"]) > 0
        authentication_id = did_document["authentication"][0]
        assert authentication_id.startswith("did:web:")

        assert "assertionMethod" in did_document
        assert isinstance(did_document["assertionMethod"], list)
        assert len(did_document["assertionMethod"]) > 0
        assertion_method_id = did_document["assertionMethod"][0]
        assert assertion_method_id.startswith("did:web:")

        assert "service" in did_document
        assert isinstance(did_document["service"], list)
        assert len(did_document["service"]) > 0
        service = did_document["service"][0]
        assert service["id"].startswith("did:web:")
        assert "#catalogue" in service["id"]
