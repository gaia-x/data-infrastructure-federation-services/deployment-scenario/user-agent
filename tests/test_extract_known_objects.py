import pytest

from agent.utils import extract_known_object_type


class TestExtractKnownObjectType:
    @pytest.mark.parametrize("object_type",[
        (["VerifiableCredential", "Location", "ServiceOffering", "ComplianceCriterion" ]),
        (["VerifiablePresentation", "Location", "ServiceOffering", "ComplianceCriterion" ]),
        (["gx:Location", "VerifiablePresentation", "ServiceOffering", "ComplianceCriterion"]),
        (["VerifiablePresentation", "gx:Location", "gx:ComplianceCriterion"]),
        ("gx:Location"),
    ])
    def test_extract_known_object_type_with_list(self, object_type):
        result = extract_known_object_type(object_type)
        assert result == "Location", "The function did not return the expected result."

    def test_extract_unknown_object_type_with_single_type(self):
        object_type = "Person:JohnDoe"
        result = extract_known_object_type(object_type)
        assert result is None, "The function did not return the expected result."

    def test_extract_known_object_type_with_invalid_input(self):
        object_type = 12345  # Invalid input
        with pytest.raises(ValueError) as e:
            extract_known_object_type(object_type)

    def test_extract_known_object_type_with_empty_list(self):
        object_type = []  # Empty list
        with pytest.raises(ValueError) as e:
            extract_known_object_type(object_type)
