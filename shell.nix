{ sources ? import ./nix/sources.nix }:     # import the sources
let
  pkgs =  import sources.nixpkgs {};
in
pkgs.mkShell {
  venvDir = "./.venv";
  buildInputs = [
      pkgs.python3Full
      pkgs.python3Packages.venvShellHook
      pkgs.pre-commit
    ];
  # Run this command, only after creating the virtual environment
  postVenvCreation = ''
    unset SOURCE_DATE_EPOCH
    pip install -r requirements-dev.txt
  '';
}
