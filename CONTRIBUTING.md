# Contributing guide

This guide explain the contibuting rules for this repository.

## Branches
### **main** is the protected branch

You cannot directly commit/push to this branch.
You need to create a Merge request to merge **develop** into **main**.

Every merge to **main** is deployed on the **demo** kubernetes cluster. Because it is used for public demo, consider this environment as a production one.

### **develop** is the trunk

Every developer can commit/push to this branch.
Yet, we strongly suggest to create a feature branch from develop, then create a merge request to integrate you work into **develop**.

Every merge to **develop** is deployed on the **dev** kubernetes cluster. This non public environment can be considered as integration/test.

## Workflow

1. Create a branch from **develop**
2. Create à Merge Request into **develop**
3. Once the Merge request is merged, you can view your changes on the dev environment
4. To push your changes to the demo environment, create à Merge Request from **develop** into **main**.
5. Once the Merge request is merged, you can view your changes on the demo environment
