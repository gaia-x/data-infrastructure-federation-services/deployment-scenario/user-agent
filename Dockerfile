ARG APP_NAME=participant-agent
ARG APP_PATH=/opt/$APP_NAME
ARG REGISTRY=public.ecr.aws/docker/library
ARG MODULE_NAME=agent
ARG PYTHON_VERSION=3.12.2
ARG POETRY_VERSION=1.8.2
ARG CI_PROJECT_URL
#
# Stage: staging
#
FROM $REGISTRY/python:$PYTHON_VERSION-slim AS staging
ARG APP_NAME
ARG APP_PATH
ARG POETRY_VERSION
ARG MODULE_NAME

ENV \
    PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONFAULTHANDLER=1 \
    POETRY_VERSION=${POETRY_VERSION} \
    POETRY_HOME="/opt/poetry" \
    POETRY_VIRTUALENVS_IN_PROJECT=true \
    POETRY_NO_INTERACTION=1

# hadolint ignore=DL3008
RUN apt-get update \
    && apt-get install --no-install-recommends -y build-essential \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN pip install --no-cache-dir "poetry==${POETRY_VERSION}"

# Import our project files
WORKDIR ${APP_PATH}
COPY ./poetry.lock ./pyproject.toml ./README.md ./
COPY ./${MODULE_NAME} ./${MODULE_NAME}

#
# Stage: build
#
FROM staging AS build
ARG APP_PATH
ARG APP_NAME

WORKDIR ${APP_PATH}
RUN poetry build --format wheel && \
    poetry export --format requirements.txt --output requirements.txt --without-hashes



#
# Stage: production
#
FROM  $REGISTRY/python:$PYTHON_VERSION-alpine AS production
ARG APP_NAME
ARG APP_PATH
ARG MODULE_NAME
ARG CI_PROJECT_URL
ARG PORT

LABEL name=$APP_NAME \
    description="It is the orchestrator of the different actions we need to do for the 23.11 demo." \
    url=$CI_PROJECT_URL                 \
    maintainer="OVHcloud"

# hadolint ignore=DL3018
RUN apk --no-cache add curl

# Get build artifact wheel and install it respecting dependency versions
WORKDIR ${APP_PATH}

COPY --from=build ${APP_PATH}/dist/*.whl ${APP_PATH}/
COPY --from=build ${APP_PATH}/requirements.txt ${APP_PATH}/


# hadolint ignore=SC2086
# hadolint ignore=DL3018
RUN apk add --no-cache \
        libressl-dev \
        musl-dev \
        libffi-dev \
        gcc \
        libc-dev \
        linux-headers && \
    pip install --no-cache-dir --upgrade pip==23.3.2 && \
    pip install --no-cache-dir ./*.whl --requirement requirements.txt && \
#    pip install --no-cache-dir  --requirement requirements.txt && \
    apk del \
        libressl-dev \
        musl-dev \
        libffi-dev \
        libc-dev \
        gcc \
        linux-headers

EXPOSE ${PORT}
#CMD [ "gunicorn", "agent.wsgi:app", "--bind 0.0.0.0:${PORT}", "--worker-class gevent", "--workers 1", "--threads 8", "--timeout 600" ]
CMD [ "python", "-u", "-m", "agent"]
