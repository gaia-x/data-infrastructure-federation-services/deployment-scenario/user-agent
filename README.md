# User Agent

It is the orchestrator of the different actions we need to do for the 22.11 demo.

## How to access endpoints ?

Each agent is publicly exposed on https://PARTICIPANT_NAME.PARENT_DOMAIN

According to the environment you want to access the PARENT_DOMAIN is specific:
* `dev.gaiax.ovh` for the dev environment
* `gaia-x.community` for the demo environment

And you can access to different endpoints

- Healthcheck : <https://PARTICIPANT_NAME.PARENT_DOMAIN/healthcheck>
- Swagger : <https://PARTICIPANT_NAME.PARENT_DOMAIN/api/doc>>

### API key

`x-api-key` header is compulsory to write (push/delete) data.

During local development mode, the expected value is set by the `API_KEY_AUTHORIZED` environment variable.
On the kubernetes cluster, this value of this is populated with a secret named `user-agent-api-key`.

For write operation on the API, you must retrieve the secret value:

```sh
kubectl -n PARTICIPANT_NAME get secret/user-agent-api-key -o=jsonpath='{.data.api_key_authorized} | base64 -d
```

Notes on the first time deployment:
This secret is managed by [Sealed secrets](https://github.com/bitnami-labs/sealed-secrets).
The encrypted value of this secret is in the [deployment files](./deployment/packages/base/deployment.yaml), inside the `SealedSecret/user-agent-api-key` resource.

To change this secret value, install kubeseal cli on your machine and run:

```sh
# Create a standard secret.yaml file
#
#
#apiVersion: v1
#kind: Secret
#metadata:
#  name: user-agent-api-key
#type: Opaque
#data:
#  api_key_authorized: XXXXXXX

kubeseal --scope cluster-wide -o yaml <secret.yaml >sealed-secret.yaml
```

Then put the content of `sealed-secret.yaml` in the [deployment files](./deployment/packages/base/deployment.yaml).

**Remember to not commit the unecrypted `secret.yaml` file!**

### IDs

#### Participants

All participants IDs are generated according to the following rule
```python
# Participants
self.num_id = hashlib.sha256(bytes(self.get_designation()+self.get_registration_number()+self.get_hqa_country(),'utf-8')).hexdigest()
# Location
Designation
# ServiceOffering
Designation
# LocatedServiceOffering

```
## Bootstrap of participants

### Demo cluster

The `PARENT_DOMAIN` for all CABs is auditor.gaia-x.community.

| CAB ID | Designation                        | Initialized |
|--------|------------------------------------|-------------|
| CAB-01 | 	Bureau Veritas Italia             |       x     |
| CAB-02 | 	KIWA                              |       x     |
| CAB-03 | 	LSTI                              |       x     |
| CAB-04 | 	ANSSI                             |       x     |
| CAB-05 | 	SGS                               |       x     |
| CAB-06 | 	S21SEC                            |       x     |
| CAB-07 | 	DNV                               |             |
| CAB-08 | 	SAP                               |       x     |
| CAB-09 | 	Cryptonet                         |             |
| CAB-10 | 	ENAC                              |       x     |
| CAB-11 | 	URS Italia S.r.l.                 |             |
| CAB-12 | 	CISPE                             |             |
| CAB-13 | 	RINA                              |             |
| CAB-14 | 	CNDCP                             |             |
| CAB-15 | 	AGID                              |             |
| CAB-16 | 	LNE                               |        x    |
| CAB-17 | 	BureauVeritasCertification        |             |
| CAB-18 | 	KPMG                              |        x    |
| CAB-19 | 	Ernst and Young Certifypoint      |        x    |
| CAB-20 | 	Ernst and Young Switzerland       |        x    |
| CAB-21 | 	Ernst and Young Germany           |        x    |
| CAB-22 | 	BDO Auditores                     |        x    |
| CAB-23 | 	Bureau Veritas Service France SAS |        x    |
| CAB-24 |  Perspective Risk                  |        x    |

Here is the list of participants:

| PARTICIPANT_NAME                  | PARENT_DOMAIN              | Initialized | Comment              |
|-----------------------------------|----------------------------|-------------|----------------------|
| gaia-x                            |    community               |  x          | Represent Gaia-X |
| abc-federation                    |  gaia-x.community          |       x     | Fake Federator which runs compliance and labelling services. Act as compliance reference manager and CAB for Notarization.
| aruba                             |  provider.gaia-x.community |       x     | |
| aws                               |  provider.gaia-x.community |       x     | |
| coretech                          |  provider.gaia-x.community |       x     | |
| gigas                             |  provider.gaia-x.community |       x     | |
| ikoula                            |  provider.gaia-x.community |       x     | |
| irideos                           |  provider.gaia-x.community |       x     | |
| jotelulu                          |  provider.gaia-x.community |       x     | |
| leaseweb                          |  provider.gaia-x.community |       x     | |
| netalia                           |  provider.gaia-x.community |       x     | |
| opiquad                           |  provider.gaia-x.community |       x     | |
| ovhcloud                          |  provider.gaia-x.community |       x     | |
| 3ds-outscale                      |  provider.gaia-x.community |       x     | |
| docaposte-contralia               |  provider.gaia-x.community |       x     | |
| docaposte-certiniomis             |  provider.gaia-x.community |       x     | |
| montblanc-iot                     |  provider.gaia-x.community |       x     | |
| dufourstorage                     |  provider.gaia-x.community |       x     | |


### Dev cluster

The `PARENT_DOMAIN` for all CABs is auditor.dev.gaiax.ovh

| CAB ID | Designation                        | Initialized |
|--------|------------------------------------|-------------|
| CAB-03 |  LSTI                              |       x     |
| CAB-04 |  ANSSI                             |       x     |

Here is the list of participants:

| PARTICIPANT_NAME                  | PARENT_DOMAIN              | Initialized | Comment              |
|-----------------------------------|----------------------------|-------------|----------------------|
| gaia-x                            |    dev.gaiax.ovh                |  x          | Represent Gaia-X |
| abc-federation                    |  dev.gaiax.ovh          |       x     | Fake Federator which runs compliance and labelling services. Act as compliance reference manager and CAB for Notarization.
| docaposte               |  provider.dev.gaiax.ovh |       x     | |
| montblanc-iot                     |  provider.dev.gaiax.ovh |       x     | |
| dufourstorage                     |  provider.dev.gaiax.ovh |       x     | |

## Technologies

- Python 3.10
- Flask API to expose endpoints

## Util commands

See the content of the [Makefile](./Makefile) to view all the util commands.
### Local dev setup

* Install python 3.10
* Install pre-commit
* Install Docker and Docker Compose
* Clone the [vc-issuer](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/vc-issuer) repository at the same level

### Local dev setup (Nix users)

For nix user, you can simply create a .envrc file for direnv:

```sh
use_nix
pre-commit install --install-hooks
```

### Local start

Docker-compose commaands are wrapped into the Makefile.

```sh
make init
# Build the local code and start the dependencies
make launch-docker
```
You can access the build user-agent at <http://localhost:5004/>
### Pre-commit and Git

On every commit, a set of pre-commit hooks **must** be runned to ensure a minimum of quality checks. pre-commit hooks are also executed on the Gitlab-ci pipelines.

Every commit will trigger pre-commit execution.

You can manually launch pre-commit:
```sh
# pre-commit only on modified files
pre-commit run
# pre-commit on all files
pre-commit run -a
```

## Contributing

See the [Contributing](./CONTRIBUTING.md) file.
