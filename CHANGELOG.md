# [1.3.0](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/compare/1.2.0...1.3.0) (2024-06-17)


### Bug Fixes

* disable linter about duplicate-code ([ca45203](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/ca452033f82dae173e55b8a367d5651024f50be6))
* disable linter about duplicate-code configuration ([89083a2](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/89083a2af21a0ac60f7747a045c3a29334d97aab))
* fix pylint configuration ([02cdcad](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/02cdcad91d1a7a65c1c392b3994a676f0b842eec))


### Features

* add authenticate method to authenticate with VC Membership and get a token. ([c849ea0](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/c849ea014e5e8cb3ad3ac7768028847300aaa468))

# [1.2.0](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/compare/1.1.3...1.2.0) (2024-05-24)


### Features

* add new vc type ([c7d9c65](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/c7d9c65c06569e175099894d42f3593c1cb83180))

## [1.1.3](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/compare/1.1.2...1.1.3) (2024-04-30)


### Bug Fixes

* default catalogue-api endpoint not set ([a5015e4](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/a5015e47733e5b9a3fd61dcc6b60906e611dce72))
* wrong behavior when VerifiableCredential is in the first entry of types ([c301956](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/c301956a3efe34fc1ffd77a8dad270b21a526a59))

## [1.1.2](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/compare/1.1.1...1.1.2) (2024-04-19)


### Bug Fixes

* add Catalogue object in allowed ontologies ([4802ccf](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/4802ccf03cf2557153ca4508974f0784f3f3736a))
* **did-document:** add catalog service only if it exists ([a16c887](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/a16c8878bf51baa231dc9e48dae932036da3a6ff))
* **did-document:** change jsw-2020 context to https://w3id.org/security/suites/jws-2020/v1 ([b30d86a](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/b30d86a4cac2953cdae26b3228b94b46cc2ff118))
* duplicate stage name in Dockerfile ([88e498e](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/88e498e9400372a857342d346234a0cf2db11340))
* error when storing an object with a hash instead of uuid in its id ([6de33d7](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/6de33d7b1a7f004c60c0019ca5f5d769056429ae))
* **store-objects:** check validity of object-id before storing objects ([3375b1e](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/3375b1e91d85c98ea4e3a2a856286383b8afe600))
* use public ecr registry instead of docker hyb ([0b2aaf7](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/0b2aaf76547df996b10d0d1dff8238fc6ffa80d0))
* wrong directory for compliance vc ([29a3e52](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/29a3e52138e3d9cfd14c5d786c9873fb8b49e31c))
* wrong directory for legal-participant compliance VC ([e9c74f7](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/e9c74f79d342047677b42ac2290a27a1ec0b3373))
* wrong directory for service-offering compliance VC ([e0198e1](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/e0198e1182b5b38007b95b7fcfa0decc312d19e1))
* wrong directory name for compliance_certificate_claim ([5475eaa](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/5475eaa767be500bbc9cb13424c68796ef567a0f))
* wrong id for vc ([72d6a76](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/72d6a761f653384e9ddab2fd759906d50d11ea6e))
* wrong id for vc ([dfe52ea](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/dfe52ea7e9512a7152117c44d7cc67c9cae8eae2))
* wrong label for legalRegistrationNumber type ([baa4873](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/baa4873234c07c99c6fe2884d833c3a579dddede))

## [1.1.1](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/compare/1.1.0...1.1.1) (2024-2-29)


### Bug Fixes

*  David Drugeon-Hamon's avatar fix: use public ecr registry instead of docker hub ([40da653](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/40da6531b41294e8ed78be99ec6694799167a1eb))

# [1.1.0](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/compare/1.0.1...1.1.0) (2024-2-29)


### Bug Fixes

* **caching:** return NO_CONTENT when clearing cache ([21ad524](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/21ad5246dd49ad1775da0d5b81d06d52bc95e6f1))
* **compliance:** Add default timeout to 120 seconds ([6ff5242](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/6ff5242ddce63f73d89a62e27adb7e8d0db6772f))
* **compliance:** Add default timeout to 120 seconds ([8f01c50](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/8f01c509ebcf3356bb1940db4cb6299e62b45ba8))
* **compliance:** wrong path where service-offering are stored ([fc253c3](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/fc253c34f72d27ae407f36712854cdeda9f108c5))
* **deployment:** wrong port of container ([7bcb2af](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/7bcb2af820c3c13e4afdfac4aa812b434714ac8b))
* **deployment:** wrong public folder ([8014dd2](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/8014dd235363228f10613eb75bbe5a48457d138c))
* **docker:** run flask app instead of gunicorn ([9e39685](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/9e3968554321cc37ec87ec6c80cfeb47abde5274))
* enable logging in info in gaiax clearing house client ([163b4fb](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/163b4fb1d4f406641c203953dfcebf38d54e9fef))


### Features

* **caching:** Add default caching for static files ([8742f03](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/8742f030501809b5d8e58cb7132932df38041a72))
* **caching:** Add route to clear cache ([2d6785d](https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/user-agent/commit/2d6785d7e4cfe7af94883d38d6d89e52fa2e1efe))
