.PHONY: init init-with-poetry clean test-ci test test-unit pre-commit-a image launch-python launch-docker

init:
	pre-commit install --install-hooks
	python3.10 -m venv .venv
	. .venv/bin/activate
	pip install -r requirements-dev.txt

init-with-poetry:
	pre-commit install --install-hooks
	poetry install

clean:
	docker-compose --env-file code/.test.env down -v
	docker-compose --env-file code/.test.env -f docker-compose-ci.yml down -v

test-ci:
	docker-compose --env-file code/.test.env -f docker-compose-ci.yml down -v
	docker-compose --env-file code/.test.env -f docker-compose-ci.yml up --renew-anon-volumes --exit-code-from pytest

test:
	docker-compose --env-file code/.test.env down -v
	docker-compose --env-file code/.test.env  up graph-db federation-catalogue vc-issuer --wait --renew-anon-volumes
	pytest

test-unit:
	pytest -m 'not integration'

pre-commit-a:
	pre-commit run -a

image:
	docker build --tag user-agent .

launch-python:
	python3 code/main.py

launch-docker:
	docker-compose --env-file code/.test.env down -v
	docker-compose --env-file code/.test.env  up --wait --renew-anon-volumes
