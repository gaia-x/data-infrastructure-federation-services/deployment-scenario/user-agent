import json

from dataclasses_json import dataclass_json
from pydantic.dataclasses import dataclass


@dataclass
@dataclass_json
class VPRequest:
    response_type: list
    client_id: list
    response_mode: list
    scope: list
    state: list
    presentation_definition: list
    client_id_scheme: list
    response_uri: list

    def get_state(self) -> str:
        return self.state[0]

    def get_presentation_definition_id(self) -> str:
        return json.loads(self.presentation_definition[0])["id"]
