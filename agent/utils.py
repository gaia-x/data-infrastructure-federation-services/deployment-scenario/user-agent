# -*- coding: utf-8 -*-
import hashlib
import hmac
import http
import json
import logging
import os
import re
import shutil
from enum import Enum
from functools import wraps
from pathlib import Path
from typing import Any, Dict, List, Optional, TypeAlias
from urllib.parse import urljoin, urlparse
from uuid import UUID

from flask import request

from agent.config import ALLOWED_OBJECT_ONTOLOGY, ALLOWED_OBJECT_TYPES, VALID_OBJECT_DIRECTORIES, settings

X_API_KEY_HEADER = "x-api-key"
JSON: TypeAlias = dict[str, "JSON"] | list["JSON"] | str | int | float | bool | None

logger = logging.getLogger(__name__)


def api_key_required(f):
    """
    Decorator to check if api key is present in HTTP header (x-api-key)

    """

    @wraps(f)
    def decorator(*args, **kwargs):
        api_key = None

        if X_API_KEY_HEADER in request.headers:
            api_key = request.headers[X_API_KEY_HEADER]

        if api_key is None:
            return {"message": "a valid api key is missing"}, http.HTTPStatus.UNAUTHORIZED

        value = api_key.strip().replace("\n", "")
        value = hashlib.sha256(value.encode()).hexdigest()
        if not hmac.compare_digest(value, settings.api_key_authorized):
            logger.warning(f"{api_key!r} != {settings.api_key_authorized!r}")
            return {"message": "api key is invalid"}, http.HTTPStatus.UNAUTHORIZED

        return f(*args, **kwargs)

    return decorator


def get_id_from_jsonld(jsonld: Dict[str, Any]) -> Optional[str]:
    """
    Get the ID from a JSON-LD dictionary.

    Args:
        jsonld: The JSON-LD dictionary.

    Returns:
        The ID value, or None if not found or not a string.
    """
    if isinstance(jsonld, dict):
        if not jsonld:
            return None
        for current in ["@id", "id"]:
            if current in jsonld and isinstance(jsonld[current], str):
                return jsonld[current]
    return None


def get_type_from_jsonld(jsonld: Dict[str, Any]) -> Optional[list[str]]:
    """
    Get the Type from a JSON-LD dictionary.

    Args:
        jsonld: The JSON-LD dictionary.

    Returns:
        The Type value, or None if not found or not a string.
    """
    if isinstance(jsonld, dict):
        if not jsonld:
            return None
        for current in ["@type", "type"]:
            if current in jsonld:
                if isinstance(jsonld[current], str):
                    return [jsonld[current]]
                return jsonld[current] if isinstance(jsonld[current], list) else None
    return None


def is_valid_store_object_parameter(path: str, ontology_type: str) -> bool:
    """
    Check if given ontology_type is allowed.

    Args:
        path: The path to store object
        ontology_type: the type of ontology of object to store

    Returns:
        True if ontology_type is allowed to be stored in path.
    """
    if not path:
        return False

    if not ontology_type:
        return False

    if not all(isinstance(i, str) for i in [path, ontology_type]):
        return False

    key = path.lower().strip()
    if key not in ALLOWED_OBJECT_ONTOLOGY:
        return False

    if ":" in ontology_type:
        ontology_type = ontology_type.split(":")[1]

    valid_type = ALLOWED_OBJECT_ONTOLOGY[key]
    return ontology_type.lower() in valid_type.lower()


class HashType(Enum):
    """
    Defines default hash algorithm types
    """

    SHA256 = "SHA256"
    SHA512 = "SHA512"
    SHA1 = "SHA1"
    MD5 = "MD5"


def compute_sha_hash_value(field: bytes, hash_type: HashType = HashType.SHA256) -> str:
    """Compute the SHA hash value of a byte string.

    :param field: The byte string to hash.
    :param hash_type: The type of hash to apply.
    :return: The hash value as a hexadecimal string.
    """
    if field is None or not isinstance(field, bytes):
        raise TypeError("Input must be of type bytes")

    if hash_type == HashType.SHA256:
        return hashlib.sha256(field).hexdigest()
    if hash_type == HashType.SHA512:
        return hashlib.sha512(field).hexdigest()
    if hash_type == HashType.SHA1:
        return hashlib.sha1(field).hexdigest()
    if hash_type == HashType.MD5:
        return hashlib.md5(field).hexdigest()

    raise TypeError("Type is not a valid HashType.")


class InvalidDataError(Exception):
    pass


def save_object_by_id(object_id: str, data: JSON):
    """
    Save an object by its ID to the appropriate directory and file.

    Args:
        object_id (str): The ID of the object.
        data (JSON): The data to be saved.

    Raises:
        ValueError: If the object_id is None or empty.

    """
    if not object_id:
        raise ValueError("Cannot find id or @id in jsonld object")

    directory, uuid_str, filename = extract_information_from_object_id(object_id=object_id)
    output_participant_directory = Path(settings.participant_folder)

    output_cred_type_directory = output_participant_directory / directory / uuid_str
    if not output_cred_type_directory.exists():
        output_cred_type_directory.mkdir(parents=True)

    output_filepath = output_cred_type_directory / filename
    save_on_disk_by_filepath(output_filepath, data)


def save_on_disk_by_filepath(filepath: str, data: JSON):
    """
    Save the given data dictionary to the specified file path on disk.

    Args:
        filepath (str): The file path where the data should be saved.
        data (JSON): The data to be saved.

    Raises:
        InvalidDataError: If the data is not a valid dictionary or an empty dictionary.

    """
    try:
        # dict[str, "JSON"] | list["JSON"] | str | int | float | bool | None
        if not isinstance(data, (dict, list, str, int, float, bool)):
            raise InvalidDataError("Invalid data type. Expected a JSON type.")

        path = Path(filepath)

        if not path.parent.exists():
            path.parent.mkdir(parents=True, exist_ok=True)

        path.write_text(json.dumps(data, indent=4, ensure_ascii=False), encoding="utf-8")

    except (FileNotFoundError, PermissionError, json.JSONDecodeError) as e:
        logger.exception("An error occurred while saving the data", exc_info=e)
        raise InvalidDataError() from e


def clean_output_directories(directories: list[str]):
    """
    Clean the output directories by removing existing directories and creating new ones if necessary.

    Args:
        directories: A list of directory paths to be cleaned.
    """
    for directory in directories:
        if os.path.isdir(directory):
            logger.info(f"Cleaning directory: {directory}")
            shutil.rmtree(directory, ignore_errors=True)
        else:
            logger.warning(f"Invalid directory path: {directory}")

        try:
            os.makedirs(directory, exist_ok=True)
        except FileExistsError:
            logger.error(f"Directory already exists: {directory}")
        else:
            logger.info(f"Successfully cleaned directory: {directory}")


def generate_object_id(base_url: str, folder: str, uuid_str: UUID, filename: Optional[str] = "data.json") -> str:
    """
    Constructs the object ID URL based on the given parameters.

    Args:
        base_url (str): The base url
        folder (str): The folder name.
        uuid_str (UUID): The UUID.
        filename (str): The filename.

    Returns:
        str: The constructed object ID URL.
    """
    if not base_url:
        raise ValueError("BaseUrl parameter cannot be None or empty")

    if not folder:
        raise ValueError("Folder parameter cannot be None or empty")

    if not uuid_str:
        raise ValueError("UUID parameter cannot be None or empty")

    base_url = base_url.rstrip("/")
    uuid_str = str(uuid_str)
    return urljoin(base_url, f"{folder}/{uuid_str}/{filename}")


def is_known_ontology(ontology_types: list[str]) -> bool:
    """
    Check if given ontology_types is managed.

    Args:
        ontology_types: the type of ontology of object to store

    Returns:
        True if ontology_types is managed.
    """
    if not ontology_types:
        return False

    if not isinstance(ontology_types, list):
        ontology_types = [ontology_types]

    if not all(isinstance(value, str) for value in ontology_types):
        return False

    allowed_ontologies = set(ALLOWED_OBJECT_ONTOLOGY.values())
    return any(ontology in allowed_ontologies for ontology in ontology_types)


def extract_known_object_type(object_type: list[str] | str) -> str:
    if not check_valid_parameters(object_type):
        raise ValueError("Invalid input format. Expected a list or a string.")

    match object_type:
        case list():
            selected_object_type = [current.split(":")[-1] if ":" in current else current for current in object_type]
            if "VerifiableCredential" in selected_object_type:
                selected_object_type.remove("VerifiableCredential")
            if "VerifiablePresentation" in selected_object_type:
                selected_object_type.remove("VerifiablePresentation")

            selected_object_type = selected_object_type[0] if selected_object_type else None
        case str():
            selected_object_type = object_type.split(":")[-1] if ":" in object_type else object_type
        case _:
            raise ValueError("Invalid input format. Expected a list or a string.")

    return selected_object_type if is_known_ontology(ontology_types=[selected_object_type]) else None


def extract_first_object_type(object_types: list[str]) -> Optional[str]:
    """
    Extracts the type of the first object from a list of object types.

    Args:
        object_types (list[str]): A list of object types.

    Returns:
        str: The type of the first object.

    Raises:
        ValueError: If the input format is invalid.

    """
    if not isinstance(object_types, list):
        raise ValueError("Invalid input format. Expected a list.")

    if not object_types:
        raise ValueError("Invalid input format. missing type or @type attribute.")

    object_type = object_types[0]

    if ":" in object_type:
        object_type = object_type.split(":")[-1]

    return object_type


def check_valid_parameters(object_type: list[str] | str) -> bool:
    if not isinstance(object_type, (str, list)):
        return False

    if isinstance(object_type, str) and object_type.strip() == "":
        return False

    if isinstance(object_type, list) and any(
        isinstance(current_type, str) and current_type.strip() == "" for current_type in object_type
    ):
        return False

    if isinstance(object_type, list) and any(isinstance(current_type, str) for current_type in object_type):
        return True

    if isinstance(object_type, str):
        return True

    return False


def is_valid_object_type(object_type: list[str] | str) -> bool:
    if check_valid_parameters(object_type):
        return is_known_ontology(ontology_types=object_type)

    return False


def check_allowed_object_type(object_type: str):
    if object_type not in ALLOWED_OBJECT_TYPES:
        msg = (
            f"{object_type} path not allowed. Only the following types are allowed: "
            f"{', '.join(ALLOWED_OBJECT_TYPES)}"
        )
        raise ValueError(msg)


def check_content_type(request_headers: dict):
    if "Content-Type" not in request_headers or "application/json" not in request_headers["Content-Type"]:
        raise ValueError(
            "You should provide a JSON Body like this one "
            "{"
            "'compliance_url': 'https://compliance.lab.gaia-x.eu/v1/api/credential-offers',"
            "'compliance_version': 'main',"
            "'vc_id': 'did:web:ovhcloud.provider.dev.gaiax.ovh:participant:"
            "68a5bbea9518e7e2ac1cc75bcc8819a7edd5c4711e073ffa4bb260034dc6423c/vc/"
            "legal-participant/gaiax-compliance-legal-participant.json',"
            "}"
        )


def check_compliance_mandatory_parameters(jsonld: JSON):
    if "compliance_url" not in jsonld:
        raise ValueError("You should provide a URL to compliance to call in compliance_url attribute")

    if "compliance_version" not in jsonld:
        raise ValueError("You should provide a URL to compliance to call in compliance_version attribute")

    if "vc_id" not in jsonld:
        raise ValueError("You should provide a URL to compliance to call in compliance_version attribute")


def load_objects_from(directories: List[Path]) -> List[JSON]:
    """
    Load objects from the given directories.

    Args:
        directories (List[Path]): The directories to load objects from.

    Returns:
        List[Union[dict, None]]: The loaded objects.
    """

    files = get_all_files_from(directories=directories)
    return_objects = []
    for file in files:
        try:
            with open(file, "r", encoding="utf-8") as current_file:
                js = json.load(current_file)
                return_objects.append(js)
        except (FileNotFoundError, PermissionError):
            continue
        except json.JSONDecodeError:
            continue
    return return_objects


def get_all_files_from(directories: List[Path]) -> List[Path]:
    """
    Get all files from the given directories.

    Args:
        directories (List[Path]): The directories to get files from.

    Returns:
        List[Path]: The list of files.
    """
    if not isinstance(directories, list):
        raise TypeError("directories must be a list")

    files: List[Path] = []
    for current_directory in directories:
        if not current_directory.exists():
            continue

        for file in current_directory.iterdir():
            if file.is_file():
                files.append(file)
            else:
                files.extend(get_all_files_from([file]))
    return files


def extract_information_from_object_id(object_id: str) -> (str, str, str):
    """
    Extract information from an object ID URL.

    Args:
        object_id (str): The object ID URL to extract information from.

    Returns:
        Tuple[str, str, str]: A tuple containing the object type, identifier, and filename extracted from the object ID.

    Raises:
        ValueError: If the object ID URL is invalid in terms of scheme, domain name, path structure, object type,
        identifier, or filename.
    """
    parsed_url = urlparse(object_id)
    if parsed_url.scheme not in ["http", "https"] or not parsed_url.netloc or not parsed_url.path:
        raise ValueError(
            f"{object_id} is invalid. It should be <scheme>://<domain name>/<object type>/<uuid-4>/data.json"
        )

    domain_name = parsed_url.netloc
    if not re.match(r"^[a-zA-Z0-9.-]+$", domain_name):
        raise ValueError(f"The domain name '{domain_name}' in the URL is invalid.")

    path_segments = parsed_url.path.strip().split("/")
    if len(path_segments) != 4:
        raise ValueError(
            f"{object_id} is invalid. It should be <scheme>://<domain name>/<object type>/<identifier>/data.json"
        )

    _, object_type, identifier, filename = path_segments

    if object_type not in VALID_OBJECT_DIRECTORIES:
        raise ValueError(f"The object-type '{object_type}' in the object ID {object_id} is unknown.")

    if not identifier.strip():
        raise ValueError(f"The identifier '{identifier}' in the object ID {object_id} is invalid.")

    if filename != "data.json":
        raise ValueError(f"The filename '{filename}' in the path of {object_id} is invalid.")

    return object_type, identifier, filename
