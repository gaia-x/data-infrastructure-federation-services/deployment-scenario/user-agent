# -*- coding: utf-8 -*-
import hashlib
import itertools
import os
from enum import Enum
from pathlib import Path
from typing import Final, Optional

from dotenv import load_dotenv
from pydantic import AnyHttpUrl, Field, field_validator
from pydantic_settings import BaseSettings

load_dotenv()


class LogLevel(str, Enum):
    critical = "CRITICAL"
    fatal = "FATAL"
    error = "ERROR"
    warning = "WARNING"
    info = "INFO"
    debug = "DEBUG"


class Environment(str, Enum):
    local = "LOCAL"
    dev = "DEV"
    prod = "PROD"
    test = "TEST"


class AllowedObjectTypes(str, Enum):
    catalogue = "catalogue"
    location = "location"
    service_offering = "service-offering"
    self_assessed_compliance_criteria_claim = "self-assessed-compliance-criteria-claim"
    self_assessed_compliance_criteria_credential = "self-assessed-compliance-criteria-credential"
    located_service_offering = "located-service-offering"
    compliance_reference = "compliance-reference"
    compliance_criterion = "compliance-criterion"
    compliance_label = "compliance-label"
    compliance_certification_scheme = "compliance-certification-scheme"
    compliance_certificate_credential = "compliance-certificate-credential"
    compliance_certificate_claim = "compliance-certificate-claim"
    third_party_compliance_certificate_credential = "third-party-compliance-certificate-credential"
    third_party_compliance_certification_scheme = "third-party-compliance-certification-scheme"
    third_party_compliance_certificate_claim = "third-party-compliance-certificate-claim"
    compliance_assessment_body = "compliance-assessment-body"
    vc = "vc"
    vp = "vp"
    data_product = "data-product"
    gaiax_legal_registration_number = "gaiax-legal-registration-number"
    gaiax_legal_participant = "legal-participant"
    gaiax_terms_and_conditions = "gaiax-terms-and-conditions"
    physical_resource = "physical-resource"
    virtual_resource = "virtual-resource"
    software_resource = "software-resource"
    instantiated_virtual_resource = "instantiated-virtual-resource"
    service_access_point = "service-access-point"
    legitimate_interest = "legitimate-interest"
    granted_label = "granted-label"
    compliance_service_offering = "compliance-service-offering"
    compliance_participant = "compliance-participant"
    organization = "organization"
    aster_x_tc = "aster-x-terms-and-conditions"
    aster_x_service_offering = "aster-x-service-offering"


class Settings(BaseSettings):
    log_level: LogLevel = Field(LogLevel.debug, description="Log level")
    environment: Environment = Field(Environment.dev, description="environment")
    api_key_authorized: str = Field(
        required=True, min_length=1, max_length=64, description="Encrypted or hashed API key"
    )
    api_port_exposed: int = Field(default=5001, ge=0, le=65535)
    participant_name: str = Field(default="ovhcloud", required=True)
    parent_domain: str = Field(default="provider.dev.gaiax.ovh", required=True)
    tmp_folder: Path = Field(default=os.path.join(Path(__file__).parent, "tmp"), required=True)
    public_folder_to_expose: Path = Field(default=os.path.join(Path(__file__).parent, "public"), required=True)
    tls_cert_path: Path = Field(default=Path("/tmp", "dummy.pem"), required=True)
    catalog_api_endpoint: Optional[str] = Field(required=False, default="")
    vc_issuer_url: AnyHttpUrl = Field(required=True, default="http://vc-issuer")
    credential_context: AnyHttpUrl = Field(required=True, default="https://www.w3.org/2018/credentials/v1")
    suites_jws_context: AnyHttpUrl = Field(required=True, default="https://w3id.org/security/suites/jws-2020/v1")
    idp_bridge_url: AnyHttpUrl = Field(required=False, default="https://idp-bridge.aster-x.demo23.gxfs.fr")

    @field_validator("api_key_authorized", mode="before")
    @classmethod
    def validate_api_key(cls, value: str):
        if value is not None:
            value = value.strip().replace("\n", "")
            value = hashlib.sha256(value.encode()).hexdigest()
        return value

    @property
    def did_url(self) -> str:
        """
        The did_url function returns a string that is the DID URL for this participant.
        The format of the DID URL is:
            did:web:[participant_name].[parent_domain]

        Args:
            self: Bind the method to an object

        Returns:
            The participant's did url
        """
        return f"did:web:{self.server_name}"

    @property
    def server_name(self) -> str:
        """
        The server_name function returns a string as
            [participant_name].[parent_domain]

        Args:
            self: Bind the method to an object

        Returns:
            The participant's server name
        """
        if self.participant_name is None:
            raise ValueError("participant_name is required")
        if self.parent_domain is None:
            raise ValueError("parent_domain is required")
        return f"{self.participant_name}.{self.parent_domain}"

    @property
    def server_root_url(self) -> str:
        """
        The server_url function returns a string as
            http[s]://[participant_name].[parent_domain]

        Args:
            self: Bind the method to an object

        Returns:
            The participant's server root url
        """

        uri_scheme = "http" if settings.environment == Environment.local else "https"
        return f"{uri_scheme}://{self.server_name}"

    @property
    def vc_context(self) -> list[str]:
        """
        The vc_context function returns list of context

        Args:
            self: Bind the method to an object

        Returns:
            The json-ld context for a VC
        """

        return [
            str(settings.credential_context),
            str(settings.suites_jws_context),
        ]

    @property
    def participant_hash(self) -> str:
        """
        The participant_hash function returns hash of participant name

        Args:
            self: Bind the method to an object

        Returns:
            The participant hash
        """

        return hashlib.sha256(bytes(settings.participant_name, "utf-8")).hexdigest()

    @property
    def participant_folder(self) -> str:
        """
        The participant_folder function returns path to participant folder

        Args:
            self: Bind the method to an object

        Returns:
            The participant folder
        """

        return str(settings.public_folder_to_expose.absolute())

    @property
    def get_gaiax_vc_directories(self) -> list[Path]:
        root_directory = settings.public_folder_to_expose
        legal_participant_directory = AllowedObjectTypes.gaiax_legal_participant.value
        legal_registration_number_directory = AllowedObjectTypes.gaiax_legal_registration_number.value
        terms_and_conditions_directory = AllowedObjectTypes.gaiax_terms_and_conditions.value

        return [
            Path(root_directory, legal_participant_directory),
            Path(root_directory, legal_registration_number_directory),
            Path(root_directory, terms_and_conditions_directory),
        ]


settings = Settings()

ALLOWED_OBJECT_ONTOLOGY: Final = {
    AllowedObjectTypes.catalogue.value: "Catalogue",
    AllowedObjectTypes.location.value: "Location",
    AllowedObjectTypes.service_offering.value: "ServiceOffering",
    AllowedObjectTypes.self_assessed_compliance_criteria_claim.value: "SelfAssessedComplianceCriteriaClaim",
    AllowedObjectTypes.self_assessed_compliance_criteria_credential.value: "SelfAssessedComplianceCriteriaCredential",
    AllowedObjectTypes.located_service_offering.value: "LocatedServiceOffering",
    AllowedObjectTypes.compliance_reference.value: "ComplianceReference",
    AllowedObjectTypes.compliance_criterion.value: "ComplianceCriterion",
    AllowedObjectTypes.compliance_label.value: "ComplianceLabel",
    AllowedObjectTypes.compliance_certification_scheme.value: "ComplianceCertificationScheme",
    AllowedObjectTypes.compliance_certificate_credential.value: "ComplianceCertificateCredential",
    AllowedObjectTypes.compliance_certificate_claim.value: "ComplianceCertificateClaim",
    AllowedObjectTypes.third_party_compliance_certificate_credential.value: "ThirdPartyComplianceCertificateCredential",
    AllowedObjectTypes.third_party_compliance_certification_scheme.value: "ThirdPartyComplianceCertificationScheme",
    AllowedObjectTypes.third_party_compliance_certificate_claim.value: "ThirdPartyComplianceCertificateClaim",
    AllowedObjectTypes.compliance_assessment_body.value: "ComplianceAssessmentBody",
    AllowedObjectTypes.vc.value: "VerifiableCredential",
    AllowedObjectTypes.vp.value: "VerifiablePresentation",
    AllowedObjectTypes.data_product.value: "DataProduct",
    AllowedObjectTypes.gaiax_legal_registration_number.value: "legalRegistrationNumber",
    AllowedObjectTypes.gaiax_legal_participant.value: "LegalParticipant",
    AllowedObjectTypes.gaiax_terms_and_conditions.value: "GaiaXTermsAndConditions",
    AllowedObjectTypes.physical_resource.value: "PhysicalResource",
    AllowedObjectTypes.virtual_resource.value: "VirtualResource",
    AllowedObjectTypes.software_resource.value: "SoftwareResource",
    AllowedObjectTypes.instantiated_virtual_resource.value: "InstantiatedVirtualResource",
    AllowedObjectTypes.service_access_point.value: "ServiceAccessPoint",
    AllowedObjectTypes.legitimate_interest.value: "LegitimateInterest",
    AllowedObjectTypes.granted_label.value: "grantedLabel",
    AllowedObjectTypes.compliance_service_offering.value: "ComplianceServiceOffering",
    AllowedObjectTypes.compliance_participant.value: "ComplianceParticipant",
    AllowedObjectTypes.organization.value: "Organization",
    AllowedObjectTypes.aster_x_tc.value: "TermsAndConditions",
    AllowedObjectTypes.aster_x_service_offering.value: "AsterXCompliance"
}
ALLOWED_OBJECT_TYPES: Final = set(ALLOWED_OBJECT_ONTOLOGY.keys())
ONTOLOGY_VC_DIRECTORIES: Final = {
    value: key
    for key, value in ALLOWED_OBJECT_ONTOLOGY.items()
    if key not in [AllowedObjectTypes.vc.value, AllowedObjectTypes.vp.value]
}

ALLOWED_OBJECT_DIRECTORIES: Final = {
    AllowedObjectTypes.location.value: ["location-json"],
    AllowedObjectTypes.service_offering.value: ["service-offering-json"],
    AllowedObjectTypes.self_assessed_compliance_criteria_claim.value: ["self-assessed-compliance-criteria-claim-json"],
    AllowedObjectTypes.self_assessed_compliance_criteria_credential.value: [
        "self-assessed-compliance-criteria-credential-json"
    ],
    AllowedObjectTypes.located_service_offering.value: ["located-service-offering-json"],
    AllowedObjectTypes.compliance_reference.value: ["compliance-reference-json"],
    AllowedObjectTypes.compliance_criterion.value: ["compliance-criterion-json"],
    AllowedObjectTypes.compliance_label.value: ["compliance-label-json"],
    AllowedObjectTypes.compliance_certification_scheme.value: ["compliance-certification-scheme-json"],
    AllowedObjectTypes.compliance_certificate_credential.value: ["compliance-certificate-credential-json"],
    AllowedObjectTypes.compliance_certificate_claim.value: ["compliance-certificate-claim-json"],
    AllowedObjectTypes.third_party_compliance_certificate_credential.value: [
        "third-party-compliance-certificate-credential-json"
    ],
    AllowedObjectTypes.third_party_compliance_certification_scheme.value: [
        "third-party-compliance-certification-scheme-json"
    ],
    AllowedObjectTypes.third_party_compliance_certificate_claim.value: [
        "third-party-compliance-certificate-claim-json"
    ],
    AllowedObjectTypes.compliance_assessment_body.value: ["compliance-assessment-body-json"],
    AllowedObjectTypes.data_product.value: ["data-product-json"],
    AllowedObjectTypes.gaiax_terms_and_conditions.value: ["gaiax-terms-and-conditions-json"],
    AllowedObjectTypes.gaiax_legal_registration_number.value: ["gaiax-legal-registration-number-json"],
    AllowedObjectTypes.aster_x_tc.value: ["aster-x-terms-and-conditions-json"],
    AllowedObjectTypes.gaiax_legal_participant.value: ["legal-participant-json"],
    AllowedObjectTypes.physical_resource.value: ["physical-resource-json"],
    AllowedObjectTypes.virtual_resource.value: ["virtual-resource-json"],
    AllowedObjectTypes.software_resource.value: ["software-resource-json"],
    AllowedObjectTypes.instantiated_virtual_resource.value: ["instantiated-virtual-resource-json"],
    AllowedObjectTypes.service_access_point.value: ["service-access-point-json"],
    AllowedObjectTypes.legitimate_interest.value: ["legitimate-interest-json"],
    AllowedObjectTypes.granted_label.value: ["granted_label-json"],
    AllowedObjectTypes.vc.value: list(ALLOWED_OBJECT_ONTOLOGY.keys()),
    AllowedObjectTypes.vp.value: ["verifiable_presentation"],
    AllowedObjectTypes.organization.value: ["organization-json"],
    AllowedObjectTypes.aster_x_service_offering.value: ["aster-x-service-offering-json"]
}

VALID_OBJECT_DIRECTORIES = set(
    itertools.chain(
        itertools.chain.from_iterable(ALLOWED_OBJECT_DIRECTORIES.values()),
        [
            f"gaiax-{AllowedObjectTypes.gaiax_legal_participant.value}",
            f"gaiax-{AllowedObjectTypes.service_offering.value}",
            f"asterx-{AllowedObjectTypes.compliance_service_offering.value}",
            f"asterx-{AllowedObjectTypes.compliance_participant.value}",
        ],
    )
)
