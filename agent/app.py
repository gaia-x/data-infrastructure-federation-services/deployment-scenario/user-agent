# -*- coding: utf-8 -*-
import logging
import os
from pathlib import Path

from flask import Flask
from flask_autoindex import AutoIndex
from flask_cors import CORS

from agent.api.cache import cache
from agent.api.views import register
from agent.config import (
    settings,
)


def create_app():
    """
    Create Flask App and initialize it
    """
    current_app = Flask(__name__)
    CORS(current_app)
    cache.init_app(current_app)

    current_app.logger.addHandler(logging.StreamHandler())
    current_app.logger.setLevel(settings.log_level.value)

    current_app.config["UPLOAD_FOLDER"] = settings.public_folder_to_expose
    current_app.logger.info(f"Creating {settings.public_folder_to_expose} ...")

    public_folder = Path(settings.public_folder_to_expose)
    public_folder.mkdir(parents=True, exist_ok=True)

    # Directory to store temporary files like csv
    current_app.config["TMP_FOLDER"] = settings.tmp_folder
    AutoIndex(current_app, browse_root=os.path.join(os.path.curdir, current_app.config["UPLOAD_FOLDER"]))
    register(current_app)

    return current_app


if __name__ == "__main__":
    app = create_app()
    app.run(host="0.0.0.0", port=settings.api_port_exposed, debug=True)
