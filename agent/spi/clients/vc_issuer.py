# -*- coding: utf-8 -*-
import hashlib
import logging
import ssl
from typing import Any, Dict, Final, Optional

import httpx

from agent.config import (
    settings,
)
from agent.spi.clients import HostNotReachable

__DEFAULT_TIMEOUT: Final = 120.0
logger = logging.getLogger(__name__)
logger.addHandler(logging.StreamHandler())
logger.setLevel(settings.log_level.value)


def _to_vc(
    vc_id: str,
    json_ld: dict[str, Any],
) -> Dict[str, Any]:
    return {
        "@context": settings.vc_context,
        "@type": ["VerifiableCredential"],
        "@id": vc_id,
        "issuer": f"{settings.did_url}",
        "credentialSubject": json_ld,
    }


def __compute_content_hash(hash_for_id, json_ld):
    sha256_hash = hashlib.sha256(bytes(str(json_ld), "utf-8"))
    return hash_for_id or sha256_hash.hexdigest()


def _to_vp(vp_id: str, verifiable_credentials: list[dict[str, Any]]) -> Dict[str, Any]:
    return {
        "@context": settings.vc_context,
        "@id": vp_id,
        "@type": ["VerifiablePresentation"],
        "verifiableCredential": verifiable_credentials,
    }


def __send_request(
    json_data: dict[str, Any],
    query_params: dict[str, Any] = None,
):
    headers = {"Content-Type": "application/json"}
    vc_issuer_url = str(settings.vc_issuer_url)
    with httpx.Client() as client:
        try:
            logger.debug(f"Sending request to {vc_issuer_url}/api/v0.9/sign")
            response = client.put(
                f"{vc_issuer_url}api/v0.9/sign",
                headers=headers,
                json=json_data,
                params=query_params,
                timeout=__DEFAULT_TIMEOUT,
            )

            logger.debug(f"VC Issuer url => {response.url}")
            logger.debug(f"Response status code => {response.status_code}")
            logger.debug(f"Response content => {response.content}")

            return response
        except (ssl.SSLCertVerificationError, httpx.ConnectError, Exception) as err:
            raise HostNotReachable(f"{vc_issuer_url} is not reachable") from err


def sign_verifiable_credentials(
    vc_id: str,
    json_ld: dict[str, Any],
) -> Any:
    verifiable_credentials = _to_vc(
        vc_id=vc_id,
        json_ld=json_ld,
    )

    return __send_request(json_data=verifiable_credentials)


def sign_verifiable_presentation(
    vp_id: str,
    verifiable_credentials: list[dict[str, Any]],
    challenge: Optional[str] = None,
    domain: Optional[str] = None,
) -> Any:
    query_params = {}

    if challenge:
        query_params["challenge"] = challenge
    if domain:
        query_params["domain"] = domain

    logger.debug(f"Query params => {query_params}")

    verifiable_presentation = _to_vp(vp_id=vp_id, verifiable_credentials=verifiable_credentials)
    return __send_request(json_data=verifiable_presentation, query_params=query_params)


def generate_verifiable_presentation(vp_id: str, verifiable_credentials: list[dict[str, Any]]) -> Any:
    return _to_vp(vp_id=vp_id, verifiable_credentials=verifiable_credentials)


def generate_vp_token(issuer: str, subject: str, membership_vc: dict[str, Any]) -> Any:
    json_data: dict[str, Any] = {"issuer": issuer, "subject": subject, "vcs": [membership_vc]}

    headers = {"Content-Type": "application/json"}
    vc_issuer_url = str(settings.vc_issuer_url)

    with httpx.Client() as client:
        try:
            logger.debug(f"Sending request to {vc_issuer_url}api/vp/issue with {json_data}")
            response = client.post(f"{vc_issuer_url}api/vp/issue", headers=headers, json=json_data)
            logger.debug(f"Response status code => {response.status_code}")
            logger.debug(f"Response content => {response.content}")

            return response
        except (ssl.SSLCertVerificationError, httpx.ConnectError, Exception) as err:
            logger.error(f"Unable to call VC Issuer: {err}")
            raise HostNotReachable(f"{vc_issuer_url} is not reachable") from err
