# -*- coding: utf-8 -*-
"""
Gaia-X Registry client functions
"""
from __future__ import annotations

import logging
import ssl

# Standard Library
from abc import ABC
from dataclasses import dataclass
from enum import Enum
from typing import Any, Final, Optional

import httpx

from agent.config import settings
from agent.spi.clients import HostNotReachable
from agent.utils import compute_sha_hash_value

logger = logging.getLogger(__name__)
logger.addHandler(logging.StreamHandler())
logger.setLevel(settings.log_level.value)

GAIA_X_CLEARING_HOUSE_REGISTRY_BASE_URL: Final = "https://registry.lab.gaia-x.eu"
GAIA_X_CLEARING_HOUSE_COMPLIANCE_BASE_URL: Final = "https://compliance.lab.gaia-x.eu"
GAIA_X_CLEARING_HOUSE_NOTARY_BASE_URL: Final = "https://registrationnumber.notary.lab.gaia-x.eu"


class GaiaXClearingHouseApiVersion(Enum):
    """
    GaiaXClearingHouseApiVersion gives valid values for compliance / registry api version
    """

    MAIN = "main"
    DEVELOPMENT = "development"
    V1 = "v1"
    V1_STAGING = "v1-staging"


class GaiaXClearingHouseRegistryException(Exception):
    """
    GaiaXClearingHouseRegistryException is raised when given Api version is not in GaiaXClearingHouseApiVersion list.
    """


class GaiaXClearingHouseComplianceException(Exception):
    """
    GaiaXClearingHouseComplianceException is raised when given Api version is not in GaiaXClearingHouseApiVersion list.
    """

    def __init__(self, status_code, msg):
        self.status_code = status_code
        self.msg = msg


class GaiaXClearingHouseNotaryException(Exception):
    """
    GaiaXClearingHouseNotaryException is raised when given Api version is not in GaiaXClearingHouseApiVersion list.
    """


@dataclass
class TermsAndConditions:
    """
    defines a TermsAndCondition object.
    """

    version: str
    text: str
    hash: Optional[str] = None


class GXCHClient(ABC):
    """
    The `GXCHClient` class is an abstract base class that provides common functionality for accessing the Gaia-X
    Clearing House API. It has two subclasses: `GXCHRegistryClient` and `GXCHNotaryClient`.
    - The `GXCHRegistryClient` class is used to retrieve the terms and conditions from the Gaia-X Clearing House
    Registry API. It has a method `get_gaix_terms_and_conditions` that sends a GET request to the API endpoint and
    returns the terms and conditions as a `TermsAndConditions` object.

    - The `GXCHNotaryClient` class is used to interact with the Gaia-X Clearing House Notary API. It has a method
    `get_registration_number_vc` that sends a POST request to the API to retrieve a registration number verification
    code (VC) for a given legal person. The response is returned as a dictionary.

    ### Example Usage
    ```python
    # Example usage of GXCHRegistryClient
    client = GXCHRegistryClient()
    terms_and_conditions = client.get_gaix_terms_and_conditions()
    print(terms_and_conditions.text)

    # Example usage of GXCHNotaryClient
    legal_person = LegalPerson(
        legalRegistrationNumber=LegalRegistrationNumber(local="123456789"),
        relatedOrganization=None,
        headquarterAddress=Address(...),
        legalAddress=Address(...),
    )

    client = GXCHNotaryClient()
    result = client.get_registration_number_vc(legal_person)
    print(result)
    ```
    """

    _DEFAULT_TIMEOUT: Final = 120.0
    _base_url: Optional[str] = None
    _default_headers: Final = {
        "Content-Type": "application/json",
    }


class GXCHRegistryClient(GXCHClient):
    """
    The `GXCHRegistryClient` class is a client for accessing the Gaia-X Clearing House Registry API. It provides a
    method `get_gaix_terms_and_conditions` to retrieve the terms and conditions from the registry.

    ### Example Usage
    ```python
    client = GXCHRegistryClient()
    terms_and_conditions = client.get_gaix_terms_and_conditions()
    print(terms_and_conditions.text)
    ```
    """

    def __init__(
        self,
        api_version: GaiaXClearingHouseApiVersion = GaiaXClearingHouseApiVersion.DEVELOPMENT,
    ):
        self._base_url = f"{GAIA_X_CLEARING_HOUSE_REGISTRY_BASE_URL}/{api_version.value}/api/"

    def get_gaix_terms_and_conditions(self, version: str = "22.10") -> TermsAndConditions:
        """
        The `get_gaix_terms_and_conditions` method is a part of the `GXCHRegistryClient` class. It retrieves the terms
        and conditions from the Gaia-X Clearing House Registry API based on the specified version. It uses the `httpx`
        library to send a GET request to the API endpoint and handles the response based on the status code. If the
        status code is 200 (HTTP.OK), it parses the response JSON and returns a `TermsAndConditions` object.
        If the status code is 400, it raises a `GaiaXClearingHouseRegistryException`. Otherwise, it also raises a
        `GaiaXClearingHouseRegistryException`.

        ### Example Usage
        ```python
        client = GXCHRegistryClient()
        terms_and_conditions = client.get_gaix_terms_and_conditions()
        print(terms_and_conditions.text)
        ```
        :param version: version of registry. By default, it is set to 22.10
        :return: GaiaXTermsAndConditions object
        :raises GaiaXRegistryException if api returns error code different from 200 (HTTP.OK)
        """

        with httpx.Client() as client:
            try:
                response = client.get(
                    url=f"{self._base_url}/termsAndConditions?version={version}",
                    timeout=self._DEFAULT_TIMEOUT,
                )

                match response.status_code:
                    case 200:
                        terms_and_conditions = response.json()

                        return TermsAndConditions(
                            text=terms_and_conditions["text"],
                            version=terms_and_conditions["version"],
                            hash=terms_and_conditions["hash"]
                            if "hash" in terms_and_conditions
                            else compute_sha_hash_value(bytes(terms_and_conditions["text"], "utf-8")),
                        )
                    case 400:
                        raise GaiaXClearingHouseRegistryException()
                    case _:
                        raise GaiaXClearingHouseRegistryException()
            except (ssl.SSLCertVerificationError, httpx.ConnectError, Exception) as err:
                raise HostNotReachable(f"{self._base_url} is not reachable") from err


class GXCHComplianceClient(GXCHClient):
    def __init__(
        self,
        base_url: str = GAIA_X_CLEARING_HOUSE_COMPLIANCE_BASE_URL,
        api_version: str = GaiaXClearingHouseApiVersion.DEVELOPMENT.value,
    ):
        self._base_url = f"{base_url}/{api_version}/api"

    def check_credential_offers(self, vp: dict[str, Any], vc_id: str) -> dict[str, Any]:
        """
        The `check_credential_offers` method is a part of the `GXCHRegistryClient` class. It calls compliance to check
        credential offers.

        :return: VC object
        :raises GaiaXRegistryException if api returns error code different from 200 (HTTP.OK)
        """
        with httpx.Client() as client:
            try:
                logger.debug(f"Sending request to {self._base_url}/credential-offers?vcid={vc_id}")

                response = client.post(
                    url=f"{self._base_url}/credential-offers?vcid={vc_id}",
                    json=vp,
                    timeout=self._DEFAULT_TIMEOUT,
                    headers=self._default_headers,
                )

                logger.debug(f"Response status code => {response.status_code}")
                logger.debug(f"Response content => {response.content}")

                match response.status_code:
                    case 200 | 201:
                        return response.json()
                    case _:
                        raise GaiaXClearingHouseComplianceException(
                            status_code=response.status_code, msg=response.json()["message"]
                        )
            except (ssl.SSLCertVerificationError, httpx.ConnectError) as err:
                raise HostNotReachable(f"{self._base_url} is not reachable") from err
