# -*- coding: utf-8 -*-
import json
import logging
import random
import ssl
from typing import Final
from urllib.parse import quote

import httpx
from httpx import Response

from agent.config import (
    settings, )
from agent.spi.clients import HostNotReachable

__DEFAULT_TIMEOUT: Final = 120.0
logger = logging.getLogger(__name__)
logger.addHandler(logging.StreamHandler())
logger.setLevel(settings.log_level.value)


class IdpBridgeClient:

    def __init__(self) -> None:
        super().__init__()
        self.url: str = settings.idp_bridge_url

    def ask_vp_request(self) -> Response:
        # json_data: dict[str, Any] = {'issuer': issuer, 'subject': subject, 'vcs': [membership_vc]}

        headers = {
            'authorizeBaseUrl': 'openid4vp://authorize',
            'responseMode': 'direct_post',
            'Content-Type': 'application/json'
        }

        payload = {
            "scopes": [
                "aster"
            ]
        }

        with httpx.Client() as client:
            try:
                logger.debug(f"Sending request to {self.url}openid4vp/aster-x/verify")
                response = client.post(f"{self.url}openid4vp/aster-x/verify", headers=headers, json=payload)
                logger.debug(f"Response status code => {response.status_code}")
                logger.debug(f"Response content => {response.content}")

                return response
            except (ssl.SSLCertVerificationError, httpx.ConnectError, Exception) as err:
                logger.error(f"Unable to call IDP Bridge: {err}")
                raise HostNotReachable(f"{self.url} is not reachable") from err

    def send_vp_response(self, state: str, vp_token: str, definition_id: str) -> Response:
        logger.debug(f"Send VP response with state={state}, vp_token={vp_token} and definition_id={definition_id}")

        presentation_submission_id = random.randint(10000, 1000000)

        presentation_submission = {
            "id": presentation_submission_id,
            "descriptor_map": [{"id": "vcard:Organization", "path": "$", "format": "ldp_vc"}],
            "definition_id": definition_id
        }

        headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
        }

        payload = f'vp_token={vp_token}&presentation_submission={quote(json.dumps(presentation_submission))}'

        with httpx.Client() as client:
            try:
                logger.debug(f"Sending request to {self.url}openid4vp/aster-x/verify/{state}")
                response = client.post(f"{self.url}openid4vp/aster-x/verify/{state}", headers=headers, content=payload)
                logger.debug(f"Response status code => {response.status_code}")
                logger.debug(f"Response content => {response.content}")

                return response
            except (ssl.SSLCertVerificationError, httpx.ConnectError, Exception) as err:
                logger.error(f"Unable to call IDP Bridge: {err}")
                raise HostNotReachable(f"{self.url} is not reachable") from err
