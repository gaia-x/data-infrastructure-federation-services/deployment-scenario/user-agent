# -*- coding: utf-8 -*-
import logging
import os
from pathlib import Path
from typing import Final

from cert_chain_resolver.api import resolve
from jwcrypto import jwk

from agent.config import settings
from agent.utils import save_on_disk_by_filepath


class DIDDocumentGenerator:
    __CERT_CHAIN_FILENAME: Final = "chain.pem"
    __WELL_KNOWN_DIR: Final = ".well-known"
    __DID_DOCUMENT_NAME: Final = "did.json"

    def __init__(
        self,
        tls_cert_path: str,
        out_folder: str,
        server_name: str,
    ) -> None:
        """
        Initialize the DIDDocumentGenerator object.

        Args:
            tls_cert_path (str): The path to the TLS certificate.
            out_folder (str): The output folder for the generated files.
            server_name (str): The server name.
        """
        if not os.path.isfile(tls_cert_path):
            raise FileNotFoundError(f"TLS certificate file not found: {tls_cert_path}")
        self.tls_cert_path = tls_cert_path

        if not os.path.exists(out_folder) or not os.path.isdir(out_folder):
            raise ValueError("The provided `out_folder` does not exist or is not a directory.")

        self.out_folder = out_folder
        self.server_name = server_name
        self.out_cert_chain_path = os.path.join(self.out_folder, ".well-known", self.__CERT_CHAIN_FILENAME)

    WELL_KNOWN_DIR = ".well-known/"

    def _retrieve_cert_chain(self) -> str:
        path = Path(self.out_folder, self.__WELL_KNOWN_DIR)
        path.mkdir(parents=True, exist_ok=True)

        with open(self.tls_cert_path, "rb") as fin:
            fb = fin.read()
            chain = resolve(fb)
            logging.info(f"Write cert chain to {self.out_cert_chain_path}")

            concatenated_certs = "\n".join(cert.export() for cert in chain)

            with open(self.out_cert_chain_path, mode="wt", encoding="utf-8") as fout:
                fout.write(concatenated_certs)

            return concatenated_certs

    @staticmethod
    def _generate_id(did_root: str, id_name: str = "") -> str:
        """
        Generate a unique identifier by combining the DID root and the given id name.

        Args:
            id_name (str): The name of the identifier.
            did_root (str): The root of the DID.

        Returns:
            str: The generated unique identifier.
        """
        if not isinstance(did_root, str):
            raise TypeError("Invalid did_root type. did_root must be a string.")

        if not did_root or not did_root.startswith("did:"):
            raise ValueError("Invalid did_root format.")

        if not isinstance(id_name, str):
            raise TypeError("Invalid id_name type. id_name must be a string.")

        return "".join([did_root, "#", id_name]) if id_name else did_root

    def _create_did_document(self, certificate_chain: str):
        out_did_document_path = Path(self.out_folder) / self.__WELL_KNOWN_DIR / self.__DID_DOCUMENT_NAME

        verification_method_id = DIDDocumentGenerator._generate_id(did_root=settings.did_url, id_name="JWK2020-RSA")
        authentication_id = DIDDocumentGenerator._generate_id(did_root=settings.did_url, id_name="JWK2020-RSA")
        assertion_method_id = DIDDocumentGenerator._generate_id(did_root=settings.did_url, id_name="JWK2020-RSA")

        # Fetch certificate
        logging.info(f"Retrieve cert for {settings.did_url}")

        # Build JWK in RFC 7797 format
        key = jwk.JWK.from_pem(bytes(certificate_chain, "UTF-8"))
        public_key_jwk = key.export(as_dict=True)
        public_key_jwk.update(
            {"alg": "PS256", "x5u": f"https://{self.server_name}/{self.__WELL_KNOWN_DIR}/{self.__CERT_CHAIN_FILENAME}"}
        )
        document = {
            "@context": [
                "https://www.w3.org/ns/did/v1",
                "https://w3id.org/security/suites/jws-2020/v1",
            ],
            "id": settings.did_url,
            "verificationMethod": [
                {
                    "id": verification_method_id,
                    "type": "JsonWebKey2020",
                    "controller": settings.did_url,
                    "publicKeyJwk": public_key_jwk,
                }
            ],
            "authentication": [authentication_id],
            "assertionMethod": [assertion_method_id],
            "service": [],
        }

        if settings.catalog_api_endpoint is not None and str(settings.catalog_api_endpoint).strip() != "":
            document["service"].append(
                {
                    "id": DIDDocumentGenerator._generate_id(did_root=settings.did_url, id_name="catalogue"),
                    "type": "LinkedDomains",
                    "serviceEndpoint": settings.catalog_api_endpoint,
                }
            )
        # Build DID document for the bare domain
        save_on_disk_by_filepath(
            filepath=str(out_did_document_path.absolute()),
            data=document,
        )

    def create_files(self):
        """
        Bootstrap provider by creating its chain cert and its did document
        """
        certificate_chain = self._retrieve_cert_chain()
        self._create_did_document(certificate_chain=certificate_chain)
