import http
import uuid
from typing import Optional
from urllib.parse import urlparse

from agent.config import ONTOLOGY_VC_DIRECTORIES, settings
from agent.spi.clients import vc_issuer
from agent.utils import (
    extract_known_object_type,
    generate_object_id,
    get_id_from_jsonld,
    get_type_from_jsonld,
    is_valid_object_type,
    save_on_disk_by_filepath,
)


def sign_verifiable_credentials(jsonld: dict) -> Optional[dict]:
    """
    Sign the verifiable credentials.

    Args:
        jsonld (dict): The JSON-LD object representing the credential subjects.

    Returns:
        dict: The signed verifiable credentials.
    """
    if not isinstance(jsonld, dict):
        raise ValueError("Invalid input format. Expected a dictionary.")

    object_id = get_id_from_jsonld(jsonld=jsonld)
    hash_value = None
    if object_id is not None:
        object_id_splitted = urlparse(object_id).path.split("/")
        _, hash_value, _ = object_id_splitted[-3:]

    object_type = get_type_from_jsonld(jsonld=jsonld)
    object_type = extract_known_object_type(object_type)
    if not is_valid_object_type(object_type=object_type):
        raise ValueError(f"Unknown object type : {object_type}.")

    vc_id = generate_object_id(
        base_url=str(settings.server_root_url),
        uuid_str=hash_value if hash_value is not None else uuid.uuid4(),
        folder=ONTOLOGY_VC_DIRECTORIES[object_type],
        filename="data.json",
    )

    response = vc_issuer.sign_verifiable_credentials(
        vc_id=vc_id,
        json_ld=jsonld,
    )

    if response.status_code == http.HTTPStatus.OK:
        signed_vc = response.json()

        path = vc_id.replace(f"{settings.server_root_url}/", "")
        save_on_disk_by_filepath(f"{settings.public_folder_to_expose}/{path}", signed_vc)

        return signed_vc
    return None


def sign_verifiable_presentation(jsonld: list) -> Optional[dict]:
    """
    Sign the verifiable presentations for a list of verifiable credentials.

    Args:
        jsonld (list): The list of verifiable credentials.

    Returns:
        dict: The signed verifiable presentation.
    """
    if not isinstance(jsonld, list):
        raise ValueError("Invalid input format. Expected a list.")

    is_correct_type = True
    for vc in jsonld:
        vc_type = get_type_from_jsonld(vc)
        is_correct_type = vc_type is not None and "VerifiableCredential" in vc_type and is_correct_type

    if not is_correct_type:
        raise ValueError("Given object is not a list of VerifiableCredential objects.")

    response = vc_issuer.sign_verifiable_presentation(
        vp_id=str(uuid.uuid4()),
        verifiable_credentials=jsonld,
    )

    return response.json() if response.status_code == http.HTTPStatus.OK else None


def generate_not_signed_verifiable_presentation(jsonld: list) -> Optional[dict]:
    """
    Generate the verifiable presentations for a list of verifiable credentials. This VP will not be signed

    Args:
        jsonld (list): The list of verifiable credentials.

    Returns:
        dict: The signed verifiable presentation.
    """
    if not isinstance(jsonld, list):
        raise ValueError("Invalid input format. Expected a list.")

    is_correct_type = True
    for vc in jsonld:
        vc_type = get_type_from_jsonld(vc)
        is_correct_type = vc_type is not None and "VerifiableCredential" in vc_type and is_correct_type

    if not is_correct_type:
        raise ValueError("Given object is not a list of VerifiableCredential objects.")

    return vc_issuer.generate_verifiable_presentation(
        vp_id=str(uuid.uuid4()),
        verifiable_credentials=jsonld,
    )
