import json
import urllib
import uuid

from agent.config import AllowedObjectTypes, settings
from agent.spi.clients.gaiax_clearing_house import GaiaXClearingHouseComplianceException, GXCHComplianceClient
from agent.spi.clients.vc_issuer import generate_verifiable_presentation
from agent.utils import (
    JSON,
    check_compliance_mandatory_parameters,
    extract_information_from_object_id,
    load_objects_from,
    save_object_by_id,
)


class GaiaXComplianceException(Exception):
    """
    GaiaXComplianceException is raised when given Api version is not in GaiaXClearingHouseApiVersion list.
    """


def load_gaiax_compliance_verifiable_credentials() -> JSON:
    return load_objects_from(directories=settings.get_gaiax_vc_directories)


def load_jsonld_from_object_id(object_id: str) -> JSON:
    object_filename = object_id.replace(str(settings.server_root_url), str(settings.public_folder_to_expose.absolute()))
    try:
        with open(object_filename, "r", encoding="utf-8") as current_file:
            return json.load(current_file)
    except (FileNotFoundError, PermissionError) as err:
        raise ValueError(f"Cannot load verifiable credential from its ID: {object_id}") from err
    except json.JSONDecodeError as err:
        raise ValueError(f"Cannot decode verifiable credential with ID: {object_id}") from err


def check_gaiax_compliance(object_type: str, jsonld: dict) -> dict:
    check_compliance_mandatory_parameters(jsonld=jsonld)
    verifiable_credentials = _prepare_verifiable_credentials(object_type, jsonld)

    hash_value = _extract_object_hash_value(object_type=object_type, jsonld=jsonld)
    vc_id = _generate_compliance_vc_id(object_type, hash_value)
    verifiable_presentation = generate_verifiable_presentation(
        vp_id=str(uuid.uuid4()), verifiable_credentials=verifiable_credentials
    )
    compliance_vc = _perform_compliance_check(jsonld, vc_id, verifiable_presentation)
    _save_compliance_vc(vc_id, compliance_vc)
    return compliance_vc


def _prepare_verifiable_credentials(object_type: str, jsonld: dict) -> list:
    verifiable_credentials = load_gaiax_compliance_verifiable_credentials()
    if object_type != AllowedObjectTypes.gaiax_legal_participant.value:
        verifiable_credential = load_jsonld_from_object_id(object_id=jsonld["vc_id"])
        verifiable_credentials.append(verifiable_credential)
    return verifiable_credentials


def _extract_object_hash_value(object_type: str, jsonld: dict) -> str:
    if object_type == AllowedObjectTypes.gaiax_legal_participant.value:
        return str(uuid.uuid4())

    object_id = jsonld["vc_id"]
    _, hash_value, _ = extract_information_from_object_id(object_id=object_id)
    return hash_value


def _generate_compliance_vc_id(object_type: str, hash_value: str) -> str:
    object_type_path = (
        object_type
        if object_type != AllowedObjectTypes.gaiax_legal_participant.value
        else AllowedObjectTypes.gaiax_legal_participant.value
    )
    return f"{settings.server_root_url}/gaiax-{object_type_path}/{hash_value}/data.json"


def _save_compliance_vc(vc_id: str, compliance_vc: dict) -> None:
    save_object_by_id(object_id=vc_id, data=compliance_vc)


def _perform_compliance_check(jsonld: dict, vc_id: str, verifiable_presentation: dict) -> dict:
    compliance_url, compliance_version = jsonld["compliance_url"], jsonld["compliance_version"]
    try:
        return GXCHComplianceClient(base_url=compliance_url, api_version=compliance_version).check_credential_offers(
            vp=verifiable_presentation,
            vc_id=urllib.parse.quote(vc_id, safe=""),
        )
    except GaiaXClearingHouseComplianceException as err:
        raise GaiaXComplianceException(err.msg) from err
