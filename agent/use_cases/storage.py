import json
import os
from json import JSONDecodeError
from pathlib import Path
from typing import List

from agent.config import ALLOWED_OBJECT_DIRECTORIES, settings
from agent.utils import (
    JSON,
    clean_output_directories,
    extract_information_from_object_id,
    get_id_from_jsonld,
    get_type_from_jsonld,
    is_valid_store_object_parameter,
    load_objects_from,
    save_object_by_id,
)


def clean_public_folder() -> bool:
    """
    Clean the public folder by removing all files and directories inside it.
    Returns True if the cleaning operation was successful, False otherwise.
    """
    if os.path.isdir(settings.public_folder_to_expose):
        try:
            files = os.listdir(settings.public_folder_to_expose)
        except OSError:
            return False

        if files:
            clean_output_directories([settings.public_folder_to_expose])
            return True
    return False


def store_file(file: JSON, object_type: str) -> str:
    json_data = _load_json_data(file)
    _validate_object_type(json_data, object_type)

    object_id = get_id_from_jsonld(jsonld=json_data)
    extract_information_from_object_id(object_id=object_id)

    save_object_by_id(object_id=object_id, data=json_data)

    return object_id


def _validate_object_type(js: JSON, object_type: str):
    ontology_types = get_type_from_jsonld(js)
    if isinstance(ontology_types, list) and not any(
        is_valid_store_object_parameter(path=object_type, ontology_type=ontology_type)
        for ontology_type in ontology_types
    ):
        raise ValueError(f"Object type is {ontology_types}, it mismatched with path {object_type}")
    if isinstance(ontology_types, str) and not is_valid_store_object_parameter(
        path=object_type, ontology_type=ontology_types
    ):
        raise ValueError(f"Object type is {ontology_types}, it mismatched with path {object_type}")


def _load_json_data(file: str) -> JSON:
    try:
        js = json.loads(file) if isinstance(file, str) else file
    except JSONDecodeError as err:
        raise ValueError("You must provide a json-ld object or a json-ld file") from err
    return js


def load_objects(object_type: str) -> List:
    """
    Load objects of a specific type from the allowed object directories.

    Args:
        object_type (str): The type of objects to load.

    Returns:
        List: The loaded objects.
    """
    if not isinstance(object_type, str):
        raise ValueError("object_type must be a string")

    if object_type not in ALLOWED_OBJECT_DIRECTORIES:
        return []

    return load_objects_from(
        directories=[
            Path(settings.public_folder_to_expose, current) for current in ALLOWED_OBJECT_DIRECTORIES[object_type]
        ]
    )


def load_objects_ids(object_type: str) -> List:
    """
    Load object ids of a specific type from the allowed object directories.

    Args:
        object_type (str): The type of objects to load.

    Returns:
        List: The loaded objects ids.
    """
    return [get_id_from_jsonld(js) for js in load_objects(object_type=object_type)]
