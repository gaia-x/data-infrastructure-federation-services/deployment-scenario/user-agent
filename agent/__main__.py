from agent.app import create_app
from agent.config import settings

if __name__ == "__main__":
    app = create_app()
    app.run(host="0.0.0.0", port=settings.api_port_exposed, debug=True)
