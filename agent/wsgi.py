import logging

from agent.app import create_app

app = create_app()

if __name__ == "__main__":
    # https://trstringer.com/logging-flask-gunicorn-the-manageable-way/
    gunicorn_logger = logging.getLogger("gunicorn.error")

    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)

    app.run()
