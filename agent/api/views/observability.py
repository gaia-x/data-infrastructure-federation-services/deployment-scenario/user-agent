import http

from flask import Blueprint, make_response

from agent.config import settings

observability_blueprint = Blueprint("observability", __name__)


@observability_blueprint.route("/healthcheck")
@observability_blueprint.route("/api/healthcheck")
def healthcheck():
    return make_response(({"status": "OK", "participant_name": settings.participant_name}, http.HTTPStatus.OK))
