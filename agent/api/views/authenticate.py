import http
import json
import logging
from urllib.parse import unquote, urlparse, parse_qs

from flask import Blueprint, current_app, make_response, request
from httpx import Response

from agent.config import settings
from agent.domain.vp_request import VPRequest
from agent.spi.clients import HostNotReachable
from agent.spi.clients.idp_bridge_client import IdpBridgeClient
from agent.spi.clients.vc_issuer import generate_vp_token

authenticate_blueprint = Blueprint("authenticate", __name__)
idp_bridge_client = IdpBridgeClient()

logger = logging.getLogger(__name__)
logger.addHandler(logging.StreamHandler())
logger.setLevel(settings.log_level.value)


def read_vp_request(response: Response) -> VPRequest:
    encoded_url = response.content.decode("utf-8")
    decoded_result = unquote(encoded_url)
    # Parsing de l'URL
    parsed_url = urlparse(decoded_result)
    # Extraction des variables de la query
    query_params = parse_qs(parsed_url.query)
    vp_request: VPRequest = VPRequest.from_dict(query_params)

    return vp_request


def read_vp_token(response: Response) -> str:
    vp_token = response.json()["vp_token"]
    return vp_token


@authenticate_blueprint.route("/api/authenticate-aster-x", methods=["POST"])
def authenticate_aster_x():
    membership_vc = request.get_json()
    current_app.logger.info(f"Request Aster-X authentication with Membership VC {membership_vc}")

    try:
        vp_request = read_vp_request(idp_bridge_client.ask_vp_request())
        logger.debug(f"VP Request received: {vp_request}")

        vp_token = read_vp_token(
            generate_vp_token(issuer=settings.did_url, subject=settings.did_url, membership_vc=membership_vc))
        logger.debug(f"VP Token generated: {vp_token}")

        response = idp_bridge_client.send_vp_response(
            state=vp_request.get_state(),
            vp_token=vp_token,
            definition_id=vp_request.get_presentation_definition_id())

        return make_response(json.loads(response.content), http.HTTPStatus.OK)
    except ValueError as e:
        return make_response((str(e), http.HTTPStatus.BAD_REQUEST))
    except HostNotReachable as e:
        return make_response((str(e), http.HTTPStatus.SERVICE_UNAVAILABLE))
