import http

from flask import Blueprint, current_app, make_response, request

from agent.utils import (
    api_key_required,
)

utils_blueprint = Blueprint("utilities", __name__)


@utils_blueprint.get("/test-api-key")
@utils_blueprint.get("/api/test-api-key")
@api_key_required
def secured_route():
    """
    Returns a test message to verify that the API key authentication is working correctly.

    Returns:
        dict: A JSON response containing a test message indicating that the API key authentication is working.
    """
    current_app.logger.info("Request received: %s", request)
    return make_response(({"msg": "It works!"}, http.HTTPStatus.OK))
