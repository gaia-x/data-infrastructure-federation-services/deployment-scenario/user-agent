import http

from flask import Blueprint, current_app, make_response, request

from agent.use_cases.compliance import GaiaXComplianceException, check_gaiax_compliance
from agent.utils import (
    api_key_required,
    check_allowed_object_type,
    check_content_type,
)

compliance_blueprint = Blueprint("compliance", __name__)


@compliance_blueprint.route("/api/call_compliance/<string:object_type>", methods=["POST"])
@api_key_required
def call_compliance(object_type):
    current_app.logger.info(f"Calling compliance for {object_type}")
    try:
        check_allowed_object_type(object_type=object_type)
        check_content_type(request_headers=request.headers)
        jsonld = request.json
        compliance_vc = check_gaiax_compliance(object_type=object_type, jsonld=jsonld)
        return make_response((compliance_vc, http.HTTPStatus.OK))
    except ValueError as err:
        return make_response(({"msg": str(err)}, http.HTTPStatus.BAD_REQUEST))
    except GaiaXComplianceException as err:
        return make_response(({"msg": str(err)}, http.HTTPStatus.CONFLICT))
