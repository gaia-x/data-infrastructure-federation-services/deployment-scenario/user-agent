from flask import Flask

from agent.api.views.authenticate import authenticate_blueprint
from agent.api.views.compliance import compliance_blueprint
from agent.api.views.observability import observability_blueprint
from agent.api.views.signer import signer_blueprint
from agent.api.views.static import static_blueprint
from agent.api.views.storage_management import storage_blueprint
from agent.api.views.utils import utils_blueprint


def register(app: Flask):
    app.register_blueprint(observability_blueprint)
    app.register_blueprint(signer_blueprint)
    app.register_blueprint(storage_blueprint)
    app.register_blueprint(static_blueprint)
    app.register_blueprint(utils_blueprint)
    app.register_blueprint(compliance_blueprint)
    app.register_blueprint(authenticate_blueprint)
