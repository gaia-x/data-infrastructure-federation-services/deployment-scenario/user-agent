import http
from json import JSONDecodeError
from typing import Final

from flask import Blueprint, Response, current_app, make_response, request

from agent.config import settings
from agent.use_cases.did_document import DIDDocumentGenerator
from agent.use_cases.storage import clean_public_folder, load_objects, load_objects_ids, store_file
from agent.utils import JSON, api_key_required, check_allowed_object_type

storage_blueprint = Blueprint("storage", __name__)


@storage_blueprint.route("/api/clean-data", methods=["DELETE"])
@api_key_required
def clean_data() -> Response:
    """
    This function cleans the data by deleting the contents of the specified directory.
    """
    current_app.logger.info("Cleaning data...")
    if clean_public_folder():
        current_app.logger.info("Data cleaned successfully.")
        return make_response(({"msg": "Data cleaned"}, http.HTTPStatus.NO_CONTENT))

    current_app.logger.info("Cannot clean public folder.")
    return make_response(({"msg": "Cannot clean public folder"}, http.HTTPStatus.UNPROCESSABLE_ENTITY))


def __allowed_file(filename: str) -> bool:
    return "." in filename and filename.rsplit(".", 1)[1].lower() in {"json"}


def __extract_file(http_request: request) -> JSON:
    FORM_FILE: Final = "objectfile"
    FORM_JSON: Final = "objectjson"

    try:
        files_from_form = http_request.files.get(FORM_FILE)

        if files_from_form and __allowed_file(http_request.files.get(FORM_FILE).filename):
            file = http_request.files[FORM_FILE]
            return file.read()

        if FORM_JSON in request.json:
            return http_request.json[FORM_JSON]

        raise ValueError("You must provide a json-ld object or a json-ld file")

    except JSONDecodeError as err:
        raise ValueError("You must provide a json-ld object or a json-ld file") from err


@storage_blueprint.route("/api/store_object/<string:object_type>", methods=["POST"])
@api_key_required
def store_object_route(object_type: str) -> Response:
    current_app.logger.debug(f"Try to store a new object of type {object_type}")

    try:
        check_allowed_object_type(object_type=object_type)
        current_file = __extract_file(http_request=request)
        object_id = store_file(file=current_file, object_type=object_type)
        return make_response(({"msg": "Object sucessfully stored", "id": object_id}, http.HTTPStatus.CREATED))
    except ValueError as err:
        return make_response(({"msg": str(err)}, http.HTTPStatus.BAD_REQUEST))


@storage_blueprint.route("/api/get_objects/<string:object_type>", methods=["GET"])
def get_objects(object_type):
    current_app.logger.info(f"Try to get objects of type {object_type}")

    try:
        check_allowed_object_type(object_type=object_type)
        return_objects = load_objects(object_type)

        return make_response(({"objects": return_objects}, http.HTTPStatus.OK))
    except ValueError as err:
        return make_response(({"msg": str(err)}, http.HTTPStatus.BAD_REQUEST))


@storage_blueprint.route("/api/get_ids/<string:object_type>", methods=["GET"])
def get_ids(object_type):
    """
    Get the IDs of objects of a specific type.

    Args:
        object_type (str): The type of objects to retrieve IDs for.

    Returns:
        dict: A dictionary containing the IDs of the objects.
    """
    try:
        check_allowed_object_type(object_type=object_type)
        return_object_ids = load_objects_ids(object_type)

        return make_response(({"ids": return_object_ids}, http.HTTPStatus.OK))
    except ValueError as err:
        return make_response(({"msg": str(err)}, http.HTTPStatus.BAD_REQUEST))


@storage_blueprint.route("/api/bootstrap-provider", methods=["POST"])
@api_key_required
def bootstrap_provider():
    current_app.logger.info(f"Start bootstrap for provider {settings.participant_name}.")

    did_document = DIDDocumentGenerator(
        tls_cert_path=settings.tls_cert_path,
        server_name=settings.server_name,
        out_folder=settings.public_folder_to_expose,
    )

    did_document.create_files()
    return make_response(
        ({"id": settings.did_url, "msg": "DID Document successfully created"}, http.HTTPStatus.CREATED)
    )
