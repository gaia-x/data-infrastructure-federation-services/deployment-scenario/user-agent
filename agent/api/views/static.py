import http

from flask import Blueprint, current_app
from flask_autoindex import send_from_directory
from flask_cors import cross_origin

from agent.api.cache import cache
from agent.utils import (
    api_key_required,
)

static_blueprint = Blueprint("static", __name__)


@static_blueprint.route("/<path:filename>")
@cross_origin()
@cache.cached(timeout=60)
def expose_directory(filename):
    """
    Exposes a directory by sending a file from the specified directory.

    Args:
        filename (str): The name of the file to be sent.

    Returns:
        make_response: The response object containing the file.

    Raises:
        NotFound: If the file does not exist in the directory.
    """
    return send_from_directory(
        directory=current_app.config["UPLOAD_FOLDER"],
        path=filename,
        as_attachment=False,
        safe_join=True,
        mimetype="application/octet-stream",
        cache_timeout=0,
    )


@static_blueprint.route("/api/cache", methods=["DELETE"])
@api_key_required
def clear_cache():
    with current_app.app_context():
        cache.clear()

    return {
        "msg": "cache cleared",
    }, http.HTTPStatus.NO_CONTENT
