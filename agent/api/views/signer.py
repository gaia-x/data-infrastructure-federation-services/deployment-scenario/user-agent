import http
import json
import uuid

from flask import Blueprint, current_app, make_response, request

from agent.spi.clients import HostNotReachable
from agent.spi.clients.vc_issuer import generate_vp_token, sign_verifiable_presentation
from agent.use_cases.signer import sign_verifiable_credentials
from agent.utils import api_key_required, settings

signer_blueprint = Blueprint("signer", __name__)


@signer_blueprint.route("/api/vc-request", methods=["PUT"])
@api_key_required
def vc_request_participant():
    current_app.logger.info(f"Request VC participant {request.json}")
    jsonld = request.json

    try:
        signed_vc = sign_verifiable_credentials(jsonld=jsonld)
        return make_response((signed_vc, http.HTTPStatus.OK, {"Content-Type": "application/ld+json"}))
    except ValueError as e:
        return make_response((str(e), http.HTTPStatus.BAD_REQUEST))
    except HostNotReachable as e:
        return make_response((e.args[0], http.HTTPStatus.SERVICE_UNAVAILABLE))


@signer_blueprint.route("/api/vp-request", methods=["PUT"])
@api_key_required
def vp_request_participant():
    verifiable_credentials = request.json
    challenge = request.args.get("challenge")
    domain = request.args.get("domain")

    current_app.logger.info(f"Request VP participant from VC {verifiable_credentials}")
    current_app.logger.info(f"Challenge {challenge}")
    current_app.logger.info(f"Domain {domain}")

    try:
        response = sign_verifiable_presentation(
            vp_id=str(uuid.uuid4()), verifiable_credentials=verifiable_credentials, challenge=challenge, domain=domain
        )
        return make_response(json.loads(response.content), http.HTTPStatus.OK)
    except ValueError as e:
        return make_response((str(e), http.HTTPStatus.BAD_REQUEST))
    except HostNotReachable as e:
        return make_response((str(e), http.HTTPStatus.SERVICE_UNAVAILABLE))


@signer_blueprint.route("/api/vp-token-request", methods=["POST"])
def vp_token_request_participant():
    membership_vc = request.get_json()
    current_app.logger.info(f"Request VP Token from Membership VC {membership_vc}")

    try:
        response = generate_vp_token(issuer=settings.did_url, subject=settings.did_url, membership_vc=membership_vc)
        return make_response(json.loads(response.content), http.HTTPStatus.OK)
    except ValueError as e:
        return make_response((str(e), http.HTTPStatus.BAD_REQUEST))
    except HostNotReachable as e:
        return make_response((str(e), http.HTTPStatus.SERVICE_UNAVAILABLE))
